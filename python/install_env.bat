py -3.6 -m venv ..\rxpa\venv
if %errorlevel% == 0 (
    ..\rxpa\venv\Scripts\pip install --no-index --find-links packages -r ..\rxpa\req.txt 
)
pause