from waitress import serve
from paste.translogger import TransLogger
from app import app

app_logged = TransLogger(app, setup_console_handler=False)
serve(app_logged, host='0.0.0.0', port=5000)
