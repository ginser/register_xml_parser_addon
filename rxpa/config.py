import os
import local_config as lc


class Config(object):
    MAX_CONTENT_LENGTH = 1024 * 1024 * 16
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'mysecretkey'

    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    BASE_DIR_UPPER = os.path.abspath(os.path.join(BASE_DIR, '..'))

    EXPLANATION_DICT_DIR = os.environ.get('EXPLANATION_DICT_DIR') or os.path.join(
        BASE_DIR_UPPER, 'explanation_dicts')
    EXPLANATION_DICTS = ['Q015', 'Q016']

    TAGS_DICT_DIR = os.environ.get('TAGS_DICT_DIR') or os.path.join(
        BASE_DIR_UPPER, 'dicts')
    RELOAD_TAGS_DICT_ON_START = True
    TAGS_DICT = dict(
        F001=dict(fields=['tf_kod, tf_okato'], disable_dateend=True),
        # F002=dict(fields=['smocod, Ogrn'], disable_dateend=True),
        F002=dict(fields=['smocod'], disable_dateend=True),
        F003=dict(fields=['mcod'], disable_dateend=True),
        F006=dict(fields=['IDVID']),
        F008=dict(fields=['IDDOC']),
        F010=dict(fields=['KOD_OKATO']),
        F011=dict(fields=['IDDoc']),
        F014=dict(fields=['Kod']),
        V002=dict(fields=['IDPR']),
        V005=dict(fields=['IDPOL'], disable_dateend=True),
        V006=dict(fields=['IDUMP']),
        V008=dict(fields=['IDVMP']),
        V009=dict(fields=['IDRMP']),
        V010=dict(fields=['IDSP']),
        #V011=dict(fields=['IDSP']),
        V012=dict(fields=['IDIZ']),
        V014=dict(fields=['IDFRMMP']),
        V016=dict(fields=['IDDT']),
        V017=dict(fields=['IDDR']),
        V018=dict(fields=['IDHVID'], disable_dateend=True),
        V019=dict(fields=['IDHM'], disable_dateend=True),
        V020=dict(fields=['IDK_PR']),
        V021=dict(fields=['IDSPEC']),
        V023=dict(fields=['K_KSG, KOEF_Z']),
        V024=dict(fields=['IDDKK']),
        V025=dict(fields=['IDPC']),
        V026=dict(fields=['K_KPG']),
        V027=dict(fields=['IDCZ']),
        V028=dict(fields=['IDVN']),
        V029=dict(fields=['IDMET']),
        O002=dict(fields=['TER'], disable_dateend=True),
        N001=dict(fields=['ID_PrOt']),
        N002=dict(fields=['ID_St']),
        N003=dict(fields=['ID_T']),
        N004=dict(fields=['ID_N']),
        N005=dict(fields=['ID_M']),
        N007=dict(fields=['ID_Mrf']),
        N008=dict(fields=['ID_Mrf']),
        N010=dict(fields=['ID_Igh']),
        N011=dict(fields=['ID_Igh']),
        N013=dict(fields=['ID_TLech']),
        N014=dict(fields=['ID_THir']),
        N015=dict(fields=['ID_TLek_L']),
        N016=dict(fields=['ID_TLek_V']),
        N017=dict(fields=['ID_TLuch']),
        N018=dict(fields=['ID_REAS']),
        N019=dict(fields=['ID_CONS']),
        N020=dict(fields=['ID_LEKP']),
    )
    MULTIPLE_FILE_CHECK_JOIN_RULES = {
        'HM, LM': dict(
            main_iterator='ZAP',
            main_iterator_key='PACIENT.ID_PAC',
            aux_iterator='./PERS',  # xpath support
            aux_iterator_key='ID_PAC'
        )
    }
    LOG_DIR = os.environ.get('LOG_DIR') or os.path.abspath(
        os.path.join(BASE_DIR_UPPER, 'log'))
    LOGGING_CONFIG = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'default': {
                'format': '[%(asctime)s] %(levelname)s in %(module)s: %('
                          'message)s',
            },
            'access': {
                'format': '%(message)s'
            }
        },
        'handlers': {
            'wsgi': {
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'formatter': 'access',
                'filename': os.path.join(LOG_DIR, 'access.log'),
                'when': 'D',
            },
            'server_file': {
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'formatter': 'default',
                'filename': os.path.join(LOG_DIR, 'server.log'),
                'when': 'D',
            },
            'xml_parser': {
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'formatter': 'default',
                'filename': os.path.join(LOG_DIR, 'xml_parser.log'),
                'when': 'D',
            },
        },
        'loggers': {
            'xml_parser': {
                'level': 'INFO',
                'handlers': ['xml_parser'],
                'propagate': False
            },
            'wsgi': {
                'level': 'DEBUG',
                'handlers': ['wsgi'],
                'propagate': False
            },
            'waitress': {
                'level': 'DEBUG',
                'handlers': ['server_file'],
                'propagate': False
            },
        },
        'root': {
            'level': 'DEBUG',
            'handlers': ['server_file']
        },
    }
