import os
from xml.etree import ElementTree as ET

from app import app


def get_explanation_dict(explanation_files):
    explanation_dict = dict()
    expl_dict_dict = app.config['EXPLANATION_DICT_DIR']

    for explanation_file in explanation_files:
        parsed_dict = _parse_explanation_file(
            os.path.join(expl_dict_dict, explanation_file + '.xml')
        )
        explanation_dict.update(parsed_dict)
    return explanation_dict


def _parse_explanation_file(explanation_file):
    parsed_dict = dict()
    with open(explanation_file, encoding='cp1251') as f:
        root = ET.fromstring(f.read())

    for zap in root.findall('./zap'):
        key = zap.find('ID_TEST').text
        value = zap.find('ID_EL').text
        parsed_dict[key] = value
    return parsed_dict
