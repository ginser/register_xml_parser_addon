﻿from flask import g

import app
import local_config as lc
from app.models import AbsModel, applicable_docs, join_file_inspection, Error
from dateutil.relativedelta import relativedelta
from datetime import datetime, date
import re, math


class XML_ROOT(AbsModel):
    def __init__(self, report_file, filename):
        super().__init__()
        self.report_file = report_file
        self.filename = filename

#test
class PERS_LIST(AbsModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        g.RegMask = [
            [1, '^[IVXLCDM]*-[А-Я][А-Я]', '\d{6}'],
            [2, '[а-яА-Я0-9- ]*', '[a-zA-Z0-9- ]*', '\d{1,8}'],
            [3, '^[IVXLCDM]*-[А-Я][А-Я]', '\d{6}'],
            [4, '[А-Я][А-Я]', '\d{7}'],
            [5, '[а-яА-Я0-9- ]*', '[a-zA-Z0-9- ]*', '\d{1,8}'],
            [6, '[А-Я][А-Я]', '\d{6}'],
            [7, '[А-Я][А-Я]', '\d{6,7}'],
            [8, '\d{2}', '\d{7}'],
            [9, '[а-яА-Я0-9- ]*', '[a-zA-Z0-9- ]*', '\d{1,12}'],
            [10, '[а-яА-Я0-9- ]*', '[a-zA-Z0-9- ]*', '\d{1,12}'],
            [11, '[а-яА-Яa-zA-Z0-9]*', '\d{1,12}'],
            [12, '[а-яА-Я0-9- ]*', '[a-zA-Z0-9- ]*', '\d{1,12}'],
            [13, '[а-яА-Я0-9- ]*', '[a-zA-Z0-9- ]*', '\d{1,12}'],
            [14, '\d{2} \d{2}', '\d{6,7}'],
            [15, '\d{2}', '\d{7}'],
            [16, '[А-Я][А-Я]', '\d{5,6}'],
            [17, '[А-Я][А-Я]', '\d{6}'],
            [18, '[а-яА-Яa-zA-Z0-9]*', ''],
            [19, '', ''],
            [20, '', ''],
            [21, '[а-яА-Яa-zA-Z0-9]*', '\d{1,12}'],
            [22, '\w{1,10}|[а-яА-Я]{1,10}', '\d{1,12}'],
            [23, '\w{1,10}|[а-яА-Я]{1,10}', '\d{1,12}'],
            [24, '[а-яА-Яa-zA-Z0-9- ]*', '\d{1,12}'],
            [25, '[А-Я][А-Я]', '\d{7}'],
            [26, '[а-яА-Яa-zA-Z0-9]*', '\d{6}'],
            [27, '[а-яА-Я0-9- ]*', '[a-zA-Z0-9- ]*', '\d{1,12}'],
            [28, '[а-яА-Яa-zA-Z0-9]*', '\d{1,12}'],
            [29, '[а-яА-Яa-zA-Z0-9]*', '\d{1,12}']]

        g.MaskList = [2, 5, 9, 10, 12, 13, 27]

    @applicable_docs('L')
    def inspection__ZGLV(self):
        if g.NE('ZGLV'):
            return Error(self, message='003F.00.3060')

    @applicable_docs('L')
    def inspection__PERS(self):
        if g.NE('PERS'):
            return Error(self, message='003F.00.3070')


class ZL_LIST(AbsModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        g.counter = 0
        g.s_sum = 0.0
        g.s_sum_mee = 0.0
        g.s_sum2 = 0.0
        g.sank_it = 0.0
        g.sumv = 0.0
        g.sump = 0.0
        g.sum_m = 0.0
        g.s_sum_max = 0.0;

    @applicable_docs('C, H, T, X, D[A-Z]')
    def inspection__FILENAME11(self):
        if g.E('ZGLV.FILENAME') and g.E('root.ZL_LIST.SCHET.YEAR'):
            p = g.S('ZGLV.FILENAME').find('_')
            if int(g.S('ZGLV.FILENAME')[p + 1:p + 3]) != (g.I('root.ZL_LIST.SCHET.YEAR') - 2000):
                return Error(self.ZGLV.FILENAME, message='002K.00.0070')

    @applicable_docs('L')
    def inspection__FILENAME2(self):
        if g.E('ZGLV.FILENAME') and g.E('root.ZL_LIST.SCHET.YEAR'):
            p = g.S('ZGLV.FILENAME').find('_')
            if int(g.S('ZGLV.FILENAME')[p + 1:p + 3]) != (g.I('root.ZL_LIST.SCHET.YEAR') - 2000):
                return Error(self.ZGLV.FILENAME, message='002K.00.0080')

    @applicable_docs('H, T, C, X, D[A-Z]')
    def inspection__FILENAME3(self):
        if g.E('ZGLV.FILENAME') and g.E('root.ZL_LIST.SCHET.YEAR'):
            p = g.S('ZGLV.FILENAME').find('_')
            if int(g.S('ZGLV.FILENAME')[p + 3:p + 5]) != g.I('root.ZL_LIST.SCHET.MONTH'):
                return Error(self.ZGLV.FILENAME, message='002K.00.0090')

    @applicable_docs('L')
    def inspection__FILENAME4(self):
        if g.E('ZGLV.FILENAME') and g.E('root.ZL_LIST.SCHET.YEAR'):
            p = g.S('ZGLV.FILENAME').find('_')
            if int(g.S('ZGLV.FILENAME')[p + 3:p + 5]) != g.I('root.ZL_LIST.SCHET.MONTH'):
                return Error(self.ZGLV.FILENAME, message='002K.00.0100')

    @applicable_docs('H, T, C, X, D[A-Z]')
    def inspection__FILENAME5(self):
        if g.E('ZGLV.FILENAME') and g.E('Pp') and g.E('S') and g.E('Np') and g.E('root.ZL_LIST.SCHET.PLAT'):
            if g.I('Pp') != g.I('S') and g.I('Np') != g.I('root.ZL_LIST.SCHET.PLAT'):
                return Error(self.ZGLV.FILENAME, message='002K.00.0050')

    @applicable_docs('L')
    def inspection__FILENAME6(self):
        if g.E('ZGLV.FILENAME') and g.E('Pp') and g.E('S') and g.E('Np') and g.E('root.ZL_LIST.SCHET.PLAT'):
            if g.I('Pp') != g.I('S') and g.I('Np') != g.I('root.ZL_LIST.SCHET.PLAT'):
                return Error(self.ZGLV.FILENAME, message='002K.00.0060')

    #  @applicable_docs('C, H, T, X')
    #  def inspection__SD_Z(self):
    #      if g.E('ZGLV.SD_Z'):
    # todo: ?
    #          if g.I('ZGLV.SD_Z') != g.counter:
    #              return Error(self.ZGLV.SD_Z, message='0')

    @applicable_docs('C, H, T, X, D[A-Z]')
    def inspection__SD_Z(self):
        if g.E('root.ZL_LIST.ZGLV.SD_Z'):
            if g.I('root.ZL_LIST.ZGLV.SD_Z') != g.counter:
                return Error(self.root.ZL_LIST.ZGLV.SD_Z, message='002K.00.0110')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SUMMAP1(self):
        if g.E('root.ZL_LIST.SCHET.SUMMAP') and g.E('root.ZL_LIST.SCHET.SUMMAV') and g.E(
                'root.ZL_LIST.SCHET.SANK_MEK') and g.E(
            'root.ZL_LIST.SCHET.SANK_MEE') and g.E('root.ZL_LIST.SCHET.SANK_EKMP'):
            if g.F('root.ZL_LIST.SCHET.SUMMAP') - ((g.F('root.ZL_LIST.SCHET.SUMMAV') - (
                    g.F('root.ZL_LIST.SCHET.SANK_MEK') + g.F('root.ZL_LIST.SCHET.SANK_MEE') + g.F(
                'root.ZL_LIST.SCHET.SANK_EKMP')))) >= 0.1:
                return Error(self.SCHET.SUMMAP, message='003K.00.0120')
        if g.E('SCHET.SUMMAV'):
            if abs(g.F('SCHET.SUMMAV') - g.sumv) >= 0.01:
                return Error(self.SCHET.SUMMAV, message='003K.00.0130')
        if g.E('root.ZL_LIST.SCHET.SUMMAP'):
            if abs(g.F('SCHET.SUMMAP') - g.sump) >= 0.01:
                return Error(self.SCHET.SUMMAP, message='003K.00.0140')

    @applicable_docs('A')
    def inspection__SUMV2(self):
        if g.E('SCHET.SUMV'):
            if g.F('SCHET.SUMV') - g.sumv >= 0.01:
                return Error(self.SCHET.SUMV, message='002K.00.0380')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SANK_IT(self):
        if g.E('root.ZL_LIST.SCHET.SANK_IT') and g.E('root.ZL_LIST.SCHET.SANK_MEK') and g.E(
                'root.ZL_LIST.SCHET.SANK_MEE') and g.E(
            'root.ZL_LIST.SCHET.SANK_EKMP'):
            if g.sank_it - (
                    g.F('root.ZL_LIST.SCHET.SANK_MEK') + g.F('root.ZL_LIST.SCHET.SANK_MEE') + g.F(
                'root.ZL_LIST.SCHET.SANK_EKMP')) >= 0.1:
                return Error(self.SCHET.SANK_IT, message='003K.00.0180')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SUMV1(self):
        if g.E('root.ZL_LIST.SCHET.SUMV'):
            if g.F('root.ZL_LIST.SCHET.SUMV') - g.sum_m >= 0.1:
                return Error(self.SCHET.SUMV, message='002K.00.0370')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__ZGLV(self):
        if g.NE('ZGLV'):
            return Error(self, message='003F.00.1900')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__SCHET(self):
        if g.NE('SCHET'):
            return Error(self, message='003F.00.1920')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__ZAP(self):
        if g.NE('ZAP'):
            return Error(self, message='003F.00.1930')


class ZGLV(AbsModel):

    @applicable_docs('R, D\d')
    def inspection__okato_oms1(self):
        if g.E('OKATO_OMS') and g.E('ZGLV.FILENAME'):
            if int(g.S('ZGLV.FILENAME')[3:4]) != g.I('OKATO_OMS'):
                return Error(self.OKATO_OMS, message='002K.00.0130')

    @applicable_docs('A')
    def inspection__okato_oms2(self):
        if g.E('OKATO_OMS') and g.E('ZGLV.FILENAME'):
            if int(g.S('ZGLV.FILENAME')[3:4]) != g.I('OKATO_OMS'):
                return Error(self.OKATO_OMS, message='002K.00.0140')

    @applicable_docs('H, T, C, R, D\w, X')
    def inspection__DATA(self):
        if g.E('DATA'):
            now = datetime.now().date()
            date = g.D('DATA')
            if date > now:
                return Error(self.DATA, message='002K.00.0010')

    @applicable_docs('L')
    def inspection__DATA1(self):
        if g.E('DATA'):
            try:
                now = datetime.now().date()
                date = g.D('DATA')
                if date > now:
                    return Error(self.DATA, message='002K.00.0020')
            except Exception as e:
                return Error(self.DATA, message='DATA ERROR')

    @applicable_docs('H, C, X, D[A-Z]')
    def inspection__FILENAME12(self):
        if g.E('ZGLV.FILENAME') and g.E('Pp') and g.E('Ni') and g.E('root.ZL_LIST.SCHET.PLAT') and g.E('M'):
            if g.I('Pi') != g.I('M') and g.I('Ni') != g.I('root.ZL_LIST.Z_SL.LPU'):
                return Error(self.FILENAME, message='002K.00.0030')

    @applicable_docs('L')
    def inspection__FILENAME2(self):
        if g.E('ZGLV.FILENAME') and g.E('Pp') and g.E('Ni') and g.E('root.ZL_LIST.SCHET.PLAT') and g.E('M'):
            if g.I('Pi') != g.I('M') and g.I('Ni') != g.I('root.ZL_LIST.Z_SL.LPU'):
                return Error(self.FILENAME, message='002K.00.0040')

    @applicable_docs('R, D\d')
    def inspection__C_OKATO11(self):
        if g.E('C_OKATO1') and g.E('ZGLV.FILENAME'):
            if int(g.S('ZGLV.FILENAME')[1:2]) != g.I('C_OKATO1'):
                return Error(self.C_OKATO1, message='002K.00.0120')

    @applicable_docs('R, D\d')
    def inspection__C_OKATO12(self):
        if g.E('C_OKATO1') and g.E('OKATO_OMS'):
            if g.I('C_OKATO1') == g.I('OKATO_OMS'):
                return Error(self.C_OKATO1, message='002K.00.0150')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__VERSION(self):
        if g.NE('VERSION'):
            return Error(self, message='003F.00.1940')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__DATA3(self):
        if g.NE('DATA'):
            return Error(self, message='003F.00.1950')

    @applicable_docs('C, H, T, D[A-Z], X')
    def inspection__FILENAME4(self):
        if g.NE('FILENAME'):
            return Error(self, message='003F.00.1960')

    @applicable_docs('L')
    def inspection__FILENAME(self):
        if g.NE('FILENAME'):
            return Error(self, message='003F.00.1970')

    @applicable_docs('C, H, T, D[A-Z], X')
    def inspection__SD_Z3(self):
        if g.NE('SD_Z'):
            return Error(self, message='003F.00.1980')

    @applicable_docs('L')
    def inspection__FILENAME113(self):
        if g.NE('FILENAME1'):
            return Error(self, message='003F.00.1990')

    @applicable_docs('R, D\d')
    def inspection__C_OKATO11(self):
        if g.NE('C_OKATO1'):
            return Error(self, message='003F.00.2000')

    @applicable_docs('A, R, D\d')
    def inspection__OKATO_OMS1(self):
        if g.NE('OKATO_OMS'):
            return Error(self, message='003F.00.2010')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__VERSION12(self):
        if g.E('VERSION'):
            check = re.fullmatch('^([a-zA-Zа-яА-Я])?\d\.\d()([a-zA-Zа-яА-Я])?', g.S('VERSION'))
            if not check:
                return Error(self.VERSION, message='004F.00.0010')

    @applicable_docs('C, H, T, X, D[A-Z]')
    def inspection__FILENAME22(self):
        if g.E('FILENAME'):
            check = re.fullmatch('^([a-zA-Z0-9_]*)$', g.S('FILENAME'))
            temp = g.S('FILENAME');
            if not check or (temp.__len__() > 28 or temp.__len__() < 13):
                return Error(self.FILENAME, message='004F.00.0020')

    @applicable_docs('L')
    def inspection__FILENAME32(self):
        if g.E('FILENAME1'):
            check = re.fullmatch('^([a-zA-Z0-9_]*)$', g.S('FILENAME1'))
            temp = g.S('FILENAME1');
            if not check or (temp.__len__() > 28 or temp.__len__() < 13):
                return Error(self.FILENAME1, message='004F.00.0030')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__DATA32(self):
        if g.E('DATA'):
            check = re.fullmatch('^\d{4}\-\d{2}\-\d{2}$', g.S('DATA'))
            if not check:
                return Error(self.DATA, message='004F.00.0040')

    @applicable_docs('D\d, R')
    def inspection__C_OCATO132(self):
        if g.E('C_OCATO1'):
            check = re.fullmatch('^\d{5}$', g.S('C_OCATO1'))
            if not check:
                return Error(self.C_OCATO1, message='004F.00.0050')

    @applicable_docs('A, D\d, R')
    def inspection__OKATO_OMS32(self):
        if g.E('OKATO)OMS'):
            check = re.fullmatch('^\d{5}$', g.S('OKATO_OMS'))
            if not check:
                return Error(self.OKATO_OMS, message='004F.00.0060')

    @applicable_docs('C, H, T, D[A-Z], X')
    def inspection__SD_Z32(self):
        if g.E('SD_Z'):
            check = re.fullmatch('^([a-zA-Zа-яА-Я0-9]{0,8})\d$', g.S('SD_Z'))
            if not check:
                return Error(self.SD_Z, message='004F.00.0070')


class SCHET(AbsModel):

    @applicable_docs('H, T, C, DP, DV, DO, DS, DU, DF')
    def inspection__CODE_MO(self):
        if g.E('CODE_MO'):
            if not g.S('CODE_MO') in app.td['F003']['mcod'] and lc.Check.ter != "Orel":
                return Error(self.CODE_MO, message='001F.00.0030')

    # @applicable_docs('C, T, D[A-Z], X')  #  not in MTR
    # def inspection__PLAT(self):
    #    if g.E('PLAT'):
    #        if not g.S('PLAT') in app.td['F002']['smocod']:
    #            return Error(self.PLAT, message='001F.00.0040')

    # @applicable_docs('DP, DV, DO, DS, DU, DF')
    # def inspection__DISP1(self):
    #    if g.E('DISP'):
    #        if not g.S('DISP') in app.td['V016']['IDDT']:
    #            return Error(self.DISP, message='ohDISP1')

    @applicable_docs('D[A-Z], X')
    def inspection__DISP2(self):
        pass
        '''if g.E('DISP'):
            if g.S('DISP') not in ['ДВ4', 'ДС1', 'ДС2']:
                return Error(self.DISP, message='003K.00.0070')
            if g.S('DISP') != 'ОПВ':
                return Error(self.DISP, message='003K.00.0080')
            if g.S('DISP') not in ['ДВ4', 'ДВ2', 'ОПВ']:
                return Error(self.DISP, message='003K.00.0090')
            if g.S('DISP') not in ['ДВ2', 'ДС3', 'ДС4', 'ПН2']:
                return Error(self.DISP, message='003K.00.0100')
            if g.S('DISP') not in ['ДВ2', 'ДВ4']:
                return Error(self.DISP, message='003K.00.0110')'''

    @applicable_docs('H, T, C, R, D\w, X')
    def inspection__YEAR1(self):
        if g.E('YEAR') and g.E('Z_SL.DATE_Z_2'):
            if all([g.E('YEAR'), g.I('YEAR') <= int(g.S('Z_SL.DATE_Z_2')[0:4])]):
                return Error(self.YEAR, message='002K.00.0160')

    @applicable_docs('A')
    def inspection__YEAR2(self):
        if g.E('YEAR') and g.E('ZGLV.DATE'):
            if g.I('YEAR') >= int(g.S('ZGLV.DATE')[0:4]):
                return Error(self.YEAR, message='002K.00.0170')

    @applicable_docs('H, T, D[A-Z], C, R, X')
    def inspection__MONTH1(self):
        if g.E('MONTH') and g.E('Z_SL.DATE_Z_2'):
            if g.I('YEAR') == int(g.S('Z_SL.DATE_Z_2')[0:4]):
                if int(g.S('MONTH')[5:7]) < int(g.S('DATE_Z_2')[5:7]):
                    return Error(self.MONTH, message='002K.00.0180')

    @applicable_docs('A')
    def inspection__MONTH2(self):
        if g.E('MONTH') and g.E('ZGLV.DATE') and g.E('YEAR'):
            if g.I('YEAR') == int(g.S('ZGLV.DATE')[0:4]):
                # todo: ?
                if int(g.S('MONTH')[5:7]) > int(g.S('ZGLV.DATE')[5:7]):
                    return Error(self.MONTH, message='002K.00.0190')

    @applicable_docs('C, D\w, H, R, T, X')
    def inspection__DSCHET(self):
        if g.E('DSCHET') and g.E('ZGLV.DATA'):
            date1 = g.D('DSCHET')
            date2 = g.D('ZGLV.DATA')
            if date1 > date2:
                return Error(self, message='002K.00.0200')

    @applicable_docs('C, D\w, H, R, T, X')
    def inspection__SANK_MEK(self):
        if g.E('SANK_MEK') and g.E('SANK.S_TIP') and g.E('SUMMAP'):
            if g.I('SANK.S_TIP') in [1, 10, 11, 12]:
                if g.I('SUMMAP') != g.s_sum:
                    return Error(self.SANK_MEK, message='003K.00.0150')

    @applicable_docs('C, D\w, H, R, T, X')
    def inspection__SANK_EKMP(self):
        if g.E('SANK_EKMP'):
            if g.I('SANK_EKMP') != g.s_sum2:
                return Error(self.SANK_EKMP, message='003K.00.0170')

    @applicable_docs('C, D\w, H, R, T, X')
    def inspection__CODE(self):
        if g.NE('CODE'):
            return Error(self, message='003F.00.2020')

    @applicable_docs('C, D[A-Z], H, T, X')
    def inspection__CODE_MO1(self):
        if g.NE('CODE_MO'):
            return Error(self, message='003F.00.2030')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__YEAR1(self):
        if g.NE('YEAR'):
            return Error(self, message='003F.00.2040')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__MONTH1(self):
        if g.NE('MONTH'):
            return Error(self, message='003F.00.2050')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__NSCHET1(self):
        if g.NE('NSCHET'):
            return Error(self, message='003F.00.2060')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__DSCHET1(self):
        if g.NE('DSCHET'):
            return Error(self, message='003F.00.2070')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__SUMMAV1(self):
        if g.NE('SUMMAV'):
            return Error(self, message='003F.00.2080')

    @applicable_docs('A, D\d, R')
    def inspection__SUMMAP11(self):
        if g.NE('SUMMAP'):
            return Error(self, message='003F.00.2090')

    @applicable_docs('D[A-Z]')
    def inspection__DISP1(self):
        if g.NE('DISP'):
            return Error(self, message='003F.00.2100')

    @applicable_docs('C, H, R, T, D\w, X')
    def inspection__CODE32(self):
        if g.E('CODE'):
            check = re.fullmatch('^([a-zA-Zа-яА-Я0-9]{0,7})\d$', g.S('CODE'))
            if not check:
                return Error(self.CODE, message='004F.00.0080')

    @applicable_docs('A,C, H, R, T, D\w, X')
    def inspection__YEAR32(self):
        if g.E('YEAR'):
            check = re.fullmatch('^\d{4}$', g.S('YEAR'))
            if not check:
                return Error(self.YEAR, message='004F.00.0090')

    @applicable_docs('A,C, H, R, T, D\w, X')
    def inspection__MONTH32(self):
        if g.E('MONTH'):
            check = re.fullmatch('^\d{1,2}$', g.S('MONTH'))
            if not check:
                return Error(self.MONTH, message='004F.00.0100')

    @applicable_docs('A,C, H, R, T, D\w, X')
    def inspection__NSCHET32(self):
        if g.E('NSCHET'):
            check = re.fullmatch('^[a-zA-Zа-яА-Я0-9`~*\-_\\/|]{1,15}$', g.S('NSCHET'))
            if not check:
                return Error(self.NSCHET, message='004F.00.0110')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__DSCHET22(self):
        if g.E('DSCHET'):
            check = re.fullmatch('^\d{4}\-\d{2}\-\d{1,2}$', g.S('DSCHET'))
            if not check:
                return Error(self.DSCHET, message='004F.00.0120')

    @applicable_docs('A, C, H, R, T, D\w, X')
    def inspection__SUMMAV22(self):
        if g.E('SUMMAV'):
            check = re.fullmatch('^[0-9|]{0,14}\d.\d{1,2}$', g.S('SUMMAV'))
            if not check and lc.Check.ter != "Orel":
                return Error(self.SUMMAV, message='004F.00.0130')

    @applicable_docs('C, H, R, T, D\w, X')
    def inspection__COMENTS22(self):
        if g.E('COMENTS'):
            check = re.fullmatch('^[a-zA-Zа-яА-Я0-9\\/| .?,!#@$%^&*()-_=+№;:\'\"]{1,250}$', g.S('COMENTS'))
            if not check:
                return Error(self.COMENTS, message='004F.00.0140')

    @applicable_docs('A, C, H, R, T, D\w, X')
    def inspection__SUMMAP22(self):
        if g.E('SUMMAP'):
            check = re.fullmatch('^[0-9]{0,14}\d.\d{1,2}$', g.S('SUMMAP'))
            if not check:
                return Error(self.SUMMAP, message='004F.00.0150')

    @applicable_docs('A, C, H, R, T, D\w, X')
    def inspection__SANK_MEK22(self):
        if g.E('SANK_MEK'):
            check = re.fullmatch('^[0-9]{0,14}\d.\d{1,2}$', g.S('SANK_MEK'))
            if not check:
                return Error(self.SANK_MEK, message='004F.00.0160')

    @applicable_docs('A, C, H, R, T, D\w, X')
    def inspection__SANK_MEE22(self):
        if g.E('SANK_MEE'):
            check = re.fullmatch('^[0-9]{0,14}\d.\d{1,2}$', g.S('SANK_MEE'))
            if not check:
                return Error(self.SANK_MEE, message='004F.00.0170')

    @applicable_docs('C, D\w, H, R, T, X')
    def inspection__SANK_MEE3(self):
        if g.E('SANK_MEE'):
            if g.F('SANK_MEE') - g.s_sum_mee > 0.1:
                return Error(self.SANK_MEE, message='003K.00.0160')

    @applicable_docs('A, C, H, R, T, D\w, X')
    def inspection__SANK_EKMP22(self):
        if g.E('SANK_EKMP'):
            check = re.fullmatch('^[0-9]{0,14}\d.\d{1,2}$', g.S('SANK_EKMP'))
            if not check:
                return Error(self.SANK_EKMP, message='004F.00.0180')


class ZAP(AbsModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @applicable_docs('D\d, R')
    def inspection__DR_DATE_Z1(self):
        if g.E('Z_SL.DATE_Z_1') and g.E('PACIENT.DR') and g.D('Z_SL.DATE_Z_1') < g.D('PACIENT.DR'):
            return Error(self, message='006F.00.0240')
        if g.E('Z_SL.NPR_DATE') and g.E('PACIENT.DR') and g.D('Z_SL.NPR_DATE') < g.D('PACIENT.DR'):
            return Error(self, message='006F.00.0311')

    @join_file_inspection
    @applicable_docs('C, H, T')
    def inspection__DR_NPR_DATE_JOIN(self):
        if g.E('Z_SL.NPR_DATE') and g.E('aux.PERS.DR') and g.D('Z_SL.NPR_DATE') < g.D('aux.PERS.DR'):
            return Error(self, message='006F.00.0311')

    @applicable_docs('C')
    def inspection__Z_SL(self):
        # согласованность уровней тегов !!
        if g.E('DS_ONK') and g.I('DS_ONK') == 0 and ((g.S('DS1') < 'C00') or (g.S('DS1') > 'D09')):
            if g.NE(Z_SL):
                return Error(self, message='003F.00.0061')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__N_ZAP1(self):
        if g.NE('N_ZAP'):
            return Error(self, message='003F.00.2110')

    @applicable_docs('C, D[A-Z], H, T, X')
    def inspection__PR_NOV1(self):
        if g.NE('PR_NOV'):
            return Error(self, message='003F.00.2120')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__PACIENT1(self):
        if g.NE('PACIENT'):
            return Error(self, message='003F.00.2130')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__Z_SL1(self):
        if g.NE('Z_SL'):
            return Error(self, message='003F.00.2140')

    @applicable_docs('A, C, H, R, T, D\w, X')
    def inspection__N_ZAP32(self):
        if g.E('N_ZAP'):
            check = re.fullmatch('^[a-zA-Zа-яА-Я0-9]{0,7}\d$', g.S('N_ZAP'))
            if not check:
                return Error(self.N_ZAP, message='004F.00.0190')

    @applicable_docs('C, H, T, D[A-Z], X')
    def inspection__PR_NOV32(self):
        if g.E('PR_NOV'):
            check = re.fullmatch('^\d$', g.S('PR_NOV'))
            if not check:
                return Error(self.PR_NOV, message='004F.00.0200')


class PR_NOV(AbsModel):
    pass


class PACIENT(AbsModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        g.counter += 1

    @applicable_docs('A, H, T, D\w, C, R, D')
    def inspection__VPOLIS(self):
        if g.E('VPOLIS'):
            if g.S('VPOLIS') not in app.td['F008']['IDDOC']:
                return Error(self.VPOLIS, message='001F.00.0060')

    @applicable_docs('H, T, D[A-Z], C, X')
    def inspection__SMO(self):
        if g.E('SMO'):
            if g.NE('SCHET.PLAT'):
                return Error(self.SMO, message='002K.00.0220')

    @applicable_docs('R, D\d')
    def inspection__W(self):
        if g.E('W') and g.E('W'):
            if g.S('W') not in app.td['V005']['IDPOL']:
                return Error(self.W, message='001F.00.0110')

    @applicable_docs('R, D\d')
    def inspection__DOCTYPE_2(self):
        if g.E('DOCTYPE') and g.NE('DOCNUM'):
            # if g.E('DOCTYPE') and g.S('DOCTYPE') not in app.td['F011']['IDDOC']:
            return Error(self.DOCTYPE, message='002K.00.0260')

    @applicable_docs('R, D\d')
    def inspection__DOCDATE(self):
        if g.E('DOCNUM') and g.NE('DOCDATE'):
            return Error(self, message='002K.00.0270')
        if g.NE('ENP'):
            if g.NE('DOCDATE'):
                return Error(self, message='003F.00.1810')
        if g.E('DOCNUM'):
            if g.NE('DOCDATE'):
                return Error(self, message='003F.00.1811')

    @applicable_docs('R, D\d')
    def inspection__DOCORG(self):
        if g.E('DOCNUM') and g.NE('DOCORG'):
            return Error(self, message='002K.00.0280')
        if g.NE('ENP'):
            if g.NE('DOCORG'):
                return Error(self, message='003F.00.1820')
        if g.E('DOCNUM'):
            if g.NE('DOCORG'):
                return Error(self, message='003F.00.1822')

    @applicable_docs('R, D\d')
    def inspection__NE_ENP(self):
        if g.NE('ENP'):
            # if g.NE('DOCORG'):
            #    return Error(self, message='003F.00.1820')
            if g.NE('DOCNUM'):
                return Error(self, message='003F.00.0320')
            if g.NE('DOCSER'):
                return Error(self, message='003F.00.0310')
            if g.NE('DOCTYPE'):
                return Error(self, message='003F.00.0300')

    @applicable_docs('R, D/d')
    def inspection__ST_OKATO(self):
        if g.E('ST_OKATO') and g.E('root.ZL_LIST.ZGLV.OKATO_OMS'):
            if g.I('ST_OKATO') != g.I('root.ZL_LIST.ZGLV.OKATO_OMS'):
                return Error(self.ST_OKATO, message='002K.00.0210')

    @applicable_docs('T, D[A-Z], C, X')
    def inspection__SMO(self):
        if g.E('SMO'):
            if g.I('SMO') in app.td['F002']['smocod']:
                return Error(self.SMO, message='001F.00.0080')
        # if g.E('root.ZL_LIST.SCHET.PLAT'):
        #    if g.NE('SMO'):
        #        return Error(self, message='003F.00.0120')

    @applicable_docs('H, T, D[A-Z], C, X')
    def inspection__SMO_OK(self):
        if g.E('SMO_OK') and g.E('VPOLIS') and g.E('ST_OKATO'):
            if g.I('VPOLIS') != 3:
                if g.I('SMO_OK') != g.I('ST_OKATO'):
                    return Error(self.SMO_OK, message='002K.00.0230')

    @applicable_docs('R, D\d')
    def inspection__DR(self):
        if g.E('DR') and g.E('Z_SL.DATE_Z_1'):
            # todo: ?
            date1 = g.D('DR')
            date2 = g.D('Z_SL.DATE_Z_1')
            if date1 > date2:
                return Error(self.DR, message='002K.00.0240')
        if g.E('DR'):
            list = [1, 4, 5, 6, 7, 8, 14, 16, 17, 26, 39]
            dr = g.D('DR')
            try:
                date = g.D('root.ZL_LIST.ZAP.Z_SL.DATE_Z_1')
                delta = relativedelta(date, dr).years
                if g.I('DOCTYPE') in list:
                    if delta < 14:
                        return Error(self.DR, message='003K.00.1030')
            except:
                pass
                # return Error(self, message='001K.00.0360')

    # L файл не содержит данных о случае,тут надо отдельно вызов совместной проверки использовать
    @applicable_docs('L')
    def inspection__DR2(self):
        if g.E('DR'):
            list = [1, 4, 5, 6, 7, 8, 14, 16, 17, 26, 39]
            dr = g.D('DR')
            date = g.D('root.ZL_LIST.ZAP.Z_SL.DATE_Z_1')
            delta = relativedelta(date, dr).years
            if g.I('DOCTYPE') in list:
                if delta < 14:
                    return Error(self.DR, message='003K.00.1020')

    # @applicable_docs('D\d, R')
    # def inspection__DR1(self):
    #     if g.E('DR') and g.E('DOCTYPE'):
    #         if g.S('DOCTYPE') in [1, 2, 4, 5, 6, 7, 8, 14, 16, 17, 26, 29]:
    #             date1 = datetime.now()
    #             date2 = g.D('DR')
    #             delta = relativedelta(date1, date2).years
    #             if delta < 14:
    #                 return Error(self.DR, message='003K.00.1020')

    @applicable_docs('R, D\d')
    def inspection__DR_P(self):
        if g.E('DR_P') and g.E('DR'):
            dr = g.D('DR')
            dr_p = g.D('DR_P')
            delta = relativedelta(dr_p, dr).years
            # todo: ?
            if delta < 14:
                return Error(self.DR_P, message='002K.00.0250')

    @applicable_docs('R, D\d')
    def inspection__DOCTYPE_3(self):
        if g.E('DOCTYPE') and g.S('DOCTYPE') not in app.td['F011']['IDDoc']:
            return Error(self.DOCTYPE, message='001F.00.0130')

    @applicable_docs('R, D\d')
    def inspection__FAM(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') != 0 or g.I('DOST') == 2:
                if g.NE('FAM'):
                    return Error(self, message='003F.00.0170')
            if g.I('NOVOR') == 0 or g.I('DOST') != 2:
                if g.NE('FAM'):
                    return Error(self, message='003F.00.0160')

    @applicable_docs('R, D\d')
    def inspection__FAM_P(self):
        if g.E('NOVOR') and g.E('DOST_P'):
            if g.I('NOVOR') != 0 or g.I('DOST_P') != 2:
                if g.NE('FAM_P'):
                    return Error(self, message='003F.00.0180')
            if g.I('NOVOR') == 0 or g.I('DOST_P') == 2:
                if g.NE('FAM_P'):
                    return Error(self, message='003F.00.0190')

    @applicable_docs('R, D\d')
    def inspection__IM(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') == 0 or g.I('DOST') != 3:
                if g.NE('IM'):
                    return Error(self, message='003F.00.0200')
            if g.I('NOVOR') != 0 or g.I('DOST') == 3:
                if g.NE('IM'):
                    return Error(self, message='003F.00.0210')

    @applicable_docs('R, D\d')
    def inspection__IM_P(self):
        if g.E('NOVOR') and g.E('DOST_P'):
            if g.I('NOVOR') != 0 or g.I('DOST_P') != 3:
                if g.NE('IM_P'):
                    return Error(self, message='003F.00.0220')
            if g.I('NOVOR') == 0 or g.I('DOST_P') == 3:
                if g.NE('IM_P'):
                    return Error(self, message='003F.00.0230')

    @applicable_docs('R, D\d')
    def inspection__OT(self):
        if g.E('NOVOR') and g.I('NOVOR') != 0 and g.E('OT'):
            return Error(self, message='003F.00.0240')
        if g.E('DOST') and (g.I('DOST') == 1) and g.E('OT'):
            return Error(self, message='003F.00.0240')

    @applicable_docs('R, D\d')
    def inspection__OT_P(self):
        if g.E('NOVOR') and g.E('DOST_P'):
            if g.I('NOVOR') == 0 or g.I('DOST_P') == 1:
                if g.NE('OT_P'):
                    return Error(self, message='003F.00.0250')

    @applicable_docs('R, D\d')
    def inspection__DOST(self):
        if g.E('NOVOR'):
            if g.I('NOVOR') == 0:
                if g.NE('FAM'):
                    if g.NE('DOST'):
                        return Error(self, message='003F.00.0260')
                if g.NE('IM'):
                    if g.NE('DOST'):
                        return Error(self, message='003F.00.0270')

    @applicable_docs('R, D\d')
    def inspection__DOST_P(self):
        if g.E('NOVOR'):
            if g.I('NOVOR') != 0:
                if g.NE('FAM_P'):
                    if g.NE('DOST_P'):
                        return Error(self, message='003F.00.0290')
                if g.NE('IM_P'):
                    if g.NE('DOST'):
                        return Error(self, message='003F.00.0280')

    @applicable_docs('C, D[A-Z], H, T, X')
    def inspection__ID_PAC11(self):
        if g.NE('ID_PAC'):
            return Error(self, message='003F.00.2150')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__VPOLIS3(self):
        if g.NE('VPOLIS'):
            return Error(self, message='003F.00.2160')

    @applicable_docs('C, D[A-Z], H, T, X')
    def inspection__NPOLIS1(self):
        if g.NE('NPOLIS'):
            return Error(self, message='003F.00.2170')

    @applicable_docs('A, C, D\w, H, R, T, X')
    def inspection__NOVOR2(self):
        if g.NE('NOVOR'):
            return Error(self, message='003F.00.2180')

    @applicable_docs('D\d, R')
    def inspection__W2(self):
        if g.NE('W'):
            return Error(self, message='003F.00.2190')

    @applicable_docs('D\d, R')
    def inspection__DR2(self):
        if g.NE('DR'):
            return Error(self, message='003F.00.2200')

    @applicable_docs('C, H, T, D[A-Z], X')
    def inspection__ID_PAC32(self):
        if g.E('ID_PAC'):
            check = re.fullmatch('^[a-zA-Z0-9|_-]{1,36}$', g.S('ID_PAC'))
            if not check:
                return Error(self.ID_PAC, message='004F.00.0210')

    @applicable_docs('A, C, H, T, D\w, X, R')
    def inspection__SPOLIS22(self):
        if g.E('SPOLIS') and g.E('VPOLIS') and g.I('VPOLIS') == 1:
            check = re.fullmatch('^[a-zA-Zа-яА-Я0-9 -_\\|/]{1,10}$', g.S('SPOLIS'))
            if not check:
                return Error(self.SPOLIS, message='004F.00.0220')

    @applicable_docs('A, C, H, T, D\w, X, R')
    def inspection__NPOLIS22(self):
        if g.E('NPOLIS') and g.E('VPOLIS') and g.I('VPOLIS') == 1:
            check = re.fullmatch('^[a-zA-Zа-яА-Я0-9 -_\\|/]{0,19}\d$', g.S('NPOLIS'))
            if not check:
                return Error(self.NPOLIS, message='004F.00.0230')

    @applicable_docs('A, C, H, T, D\w, X, R')
    def inspection__NPOLIS23(self):
        if g.E('NPOLIS') and g.E('VPOLIS') and g.I('VPOLIS') == 2:
            check = re.fullmatch('^\d{9}$', g.S('NPOLIS'))
            if not check:
                return Error(self.NPOLIS, message='004F.00.0240')

    @applicable_docs('A, C, H, T, D\w, X, R')
    def inspection__NPOLIS24(self):
        if g.E('NPOLIS') and g.E('VPOLIS') and g.I('VPOLIS') == 3:
            if g.S('NPOLIS'):
                check = re.fullmatch('^\d{16}$', g.S('NPOLIS'))
                if not check:
                    return Error(self.NPOLIS, message='004F.00.0250')


class ENP(AbsModel):
    pass


class NOVOR(AbsModel):
    pass


class Z_SL(AbsModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # g.counter += 1

    #    @applicable_docs('A, C, R, H, D, T, X, D\w')
    #    def inspection__IDCASE(self):
    #        if g.NE('IDCASE') and g. parent.E('ZAP'):
    #            return Error(self, message='001K.00.0310')

    @applicable_docs('C, R, H, D, T, X, D\w')
    def inspection__LPU_1(self):
        if g.NE('LPU') and g.E('IDCASE'):  # and g.S('LPU') not in app.td['F003']['mcod']:
            return Error(self, message='003F.00.2250')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SUMV3(self):
        if g.E('SUMV'):
            try:
                g.sumv += g.F('SUMV')
            except:
                return Error(self, message='004F.00.0630')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SANK_IT3(self):
        if g.E('SANK_IT'):
            g.sank_it += g.F('SANK_IT')
        if g.E('SANK_IT'):
            if (g.F('SANK_IT') - g.s_sum_max) > 0.1:
                return Error(self.SANK_IT, message='003K.00.0250')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SUMP3(self):
        if g.E('SUMP'):
            g.sump += g.F('SUMP')

    @applicable_docs('H, T, C, R, D')
    def inspection__KD_Z(self):
        if g.E('KD_Z') and g.E('USL_OK') and g.E('DATE_Z_1') and g.E('DATE_Z_2'):
            if g.I('USL_OK') in [1, 2]:
                date1 = g.D('DATE_Z_1')
                date2 = g.D('DATE_Z_2')
                delta = 1 if date1 == date2 else (date2 - date1).days
                if g.I('USL_OK') == 2:  # Дневной стационар
                    delta += 1
                if g.I('KD_Z') == 0 or g.I('KD_Z') > delta:
                    return Error(self.KD_Z, message='003K.00.0440')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__USL_OK(self):
        if g.E('USL_OK') and g.S('USL_OK') not in app.td['V006']['IDUMP']:
            return Error(self.USL_OK, message='001F.00.0160')

    @applicable_docs('H, T,  C, R, D\d')
    def inspection__VIDPOM1(self):
        if g.E('VIDPOM') and g.E('USL_OK'):
            list1 = [2, 21, 22, 23]
            list2 = [1, 2, 11, 12, 13, 4, 14]
            list3 = [12, 13, 14, 31, 32, 33]
            list4 = [3, 31, 32, 33]
            if g.I('USL_OK') == 4:
                if g.I('VIDPOM') not in list1:
                    return Error(self.VIDPOM, message='003K.00.0290')
            elif g.I('USL_OK') == 3:
                if g.I('VIDPOM') not in list2:
                    return Error(self.VIDPOM, message='003K.00.0300')
            elif g.I('USL_OK') == 2:
                if g.I('VIDPOM') not in list3:
                    return Error(self.VIDPOM, message='003K.00.0310')
            elif g.I('USL_OK') == 1:
                if g.I('VIDPOM') not in list4:
                    return Error(self.VIDPOM, message='003K.00.0320')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__FOR_POM(self):
        if g.E('FOR_POM') and g.E('USL_OK'):
            if g.E('FOR_POM') and g.I('USL_OK') == 2:
                if g.I('FOR_POM') != 3:
                    return Error(self.FOR_POM, message='003K.00.0370')
            if g.E('FOR_POM') and g.I('USL_OK') == 3:
                if g.I('FOR_POM') not in [2, 3]:
                    return Error(self.FOR_POM, message='003K.00.0380')
            if g.E('FOR_POM') and g.I('USL_OK') == 4:
                if g.I('FOR_POM') not in [1, 2]:
                    return Error(self.FOR_POM, message='003K.00.0390')

    # @applicable_docs('T, C, R, D')
    # def inspection__NPR_MO1(self):
    #     if g.E('NPR_MO') and g.E('FOR_POM') and g.E('USL_OK') and g.E('DS1') and g.E('DS2') and g.E('LPU'):
    #         if g.I('FOR_POM') == 3 and g.I('USL_OK') == 1 or g.I('USL_OK') == 2:
    #             if g.S('NPR_MO') not in app.td['F003']['mcod']:
    #                 return Error(self.NPR_MO, message='ohNPR_MO')
    #         elif 'C00' <= g.S('DS1') <= 'C97' or (
    #                 g.S('DS1') == 'D70' and (
    #                 g.S('DS2') == 'C97' or 'C00' <= g.S('DS2') <= 'C80'
    #         ) and g.I('NPR_MO') != g.I('LPU')
    #         ):
    #             if g.S('NPR_MO') not in app.td['F003']['mcod']:
    #                 return Error(self.NPR_MO, message='ohNPR_MO2')

    @applicable_docs('C,  H, R, T, D\d')
    def inspection__NPR_MO(self):
        if g.E('NPR_DATE') and g.NE('NPR_MO'):
            return Error(self, message='002K.00.0290')

        if g.E('NPR_DATE') and g.NE('NPR_MO'):
            return Error(self, message='003F.00.0401')

        if g.E('NPR_DATE') and g.E('NPR_MO'):
            if g.S('NPR_MO') not in app.td['F003']['mcod']:
                return Error(self.NPR_MO, message='001F.00.0190')
        if g.E('USL_OK') and g.E('FOR_POM'):
            if g.I('USL_OK') == 2 or all(
                    [g.I('FOR_POM') == 3, g.I('USL_OK') == 1]):
                if g.NE('NPR_MO'):
                    return Error(self, message='003F.00.0400')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__LPU_2(self):
        if g.E('LPU') and g.E('IDCASE'):
            if g.S('LPU') not in app.td['F003']['mcod'] and lc.Check.ter != "Orel":
                return Error(self, message='001F.00.0200')
                # return Error(self, message='001F.00.0200+'+g.S('LPU'))

    @applicable_docs('C,  H, R, T, D\d')
    def inspection__RSLT1(self):
        if g.E('RSLT') and g.S('RSLT') not in app.td['V009']['IDRMP']:
            return Error(self.RSLT, message='001F.00.0210')

    @applicable_docs('R, D\d')
    def inspection__RSLT2(self):
        lst = [317, 321, 332, 343, 347]
        if g.E('RSLT') and g.I('RSLT') in lst:
            return Error(self.RSLT, message='002K.00.0360')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__RSLT3(self):
        list1 = [105, 106]
        list2 = [205, 206]
        list3 = [313]
        list4 = [405, 406]
        if g.E('RSLT') and g.E('ISHOD') and g.E('USL_OK'):
            if g.I('RSLT') in list1:
                if g.I('ISHOD') != 104:
                    return Error(self.RSLT, message='003K.00.0331')
            elif g.I('RSLT') in list2:
                if g.I('ISHOD') != 204:
                    return Error(self.RSLT, message='003K.00.0341')
            elif g.I('RSLT') in list3:
                if g.I('ISHOD') != 305:
                    return Error(self.RSLT, message='003K.00.0351')
            elif g.I('RSLT') in list4:
                if g.I('ISHOD') != 403:
                    return Error(self.RSLT, message='003K.00.0361')
            if g.I('USL_OK') == 1:
                if g.I('RSLT') < 100 or g.I('RSLT') > 200:
                    return Error(self.RSLT, message='003K.00.0450')
            elif g.I('USL_OK') == 2:
                if g.I('RSLT') < 200 or g.I('RSLT') > 300:
                    return Error(self.RSLT, message='003K.00.0460')
            elif g.I('USL_OK') == 3:
                if g.I('RSLT') < 300 or g.I('RSLT') > 400:
                    return Error(self.RSLT, message='003K.00.0470')
            elif g.I('USL_OK') == 4:
                if g.I('RSLT') < 400 or g.I('RSLT') > 500:
                    return Error(self.RSLT, message='003K.00.0480')

    @applicable_docs('R, D\d')
    def inspection__RSLT4(self):
        list1 = [305, 308, 314, 315, 317, 318, 321, 322, 323, 324, 325, 332, 333, 334, 335, 336, 343, 344, 347, 348,
                 349,
                 350, 351, 353, 355, 356, 357, 358, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373,
                 374]
        list2 = [305, 308, 314, 315]
        if g.E('RSLT') and g.E('ISHOD'):
            if g.I('ISHOD') == 306:
                if g.I('RSLT') not in list1:
                    return Error(self.RSLT, message='003K.00.0191')
                if g.I('RSLT') not in list2:
                    return Error(self.RSLT, message='003K.00.0195')

    @applicable_docs('C, H')
    def inspection__RSLT5(self):
        list1 = [301, 305, 308, 314, 315]
        if g.E('RSLT') and g.E('ISHOD'):
            if g.I('ISHOD') == 306:
                if g.I('RSLT') not in list1:
                    return Error(self.RSLT, message='003K.00.0196')

    @applicable_docs('R,  C, H, T, D\d')
    def inspection__ISHOD2(self):
        list2 = [407, 408, 409, 410, 411, 412, 413, 414, 417]
        if g.E('ISHOD') and g.E('RSLT'):
            if g.I('RSLT') in list2 and ((g.I('ISHOD') != 402 and lc.Check.ter != "Smol") or (
                    not g.I('ISHOD') in (401, 402) and lc.Check.ter == "Smol")):
                return Error(self.ISHOD, message='003K.00.0200')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__ISHOD(self):
        if g.E('ISHOD') and g.E('RSLT'):
            list1 = [102, 103, 104, 105, 106, 109]
            list2 = [202, 203, 204, 205, 206, 207, 208]
            if g.S('ISHOD') not in app.td['V012']['IDIZ']:
                return Error(self.ISHOD, message='001F.00.0220')
            elif g.I('RSLT') in list1:
                if g.I('ISHOD') == 101:
                    return Error(self.ISHOD, message='003K.00.0210')
            elif g.I('RSLT') in list2:
                if g.I('ISHOD') == 201:
                    return Error(self.ISHOD, message='003K.00.0220')

    @applicable_docs('X, D[A-Z]')
    def inspection__IDSP1(self):
        lst = [25, 28, 29, 30, 31, 44]
        if g.E('IDSP') and g.I('IDSP') not in lst:
            return Error(self.IDSP, message='003K.00.0230')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__IDSP2(self):
        if g.NE('IDSP') and g.E('IDCASE'):
            return Error(self, message='001K.00.0410')
        if g.E('IDSP') and g.S('IDSP') not in app.td['V010']['IDSP']:
            return Error(self.IDSP, message='001F.00.0230')

    @applicable_docs(' R, D\d')
    def inspection__IDSP3(self):
        lst = [24, 28, 29, 30, 32, 33]
        if g.E('IDSP') and g.I('IDSP') not in lst:
            return Error(self.IDSP, message='003K.00.0400')

    @applicable_docs('X, D[A-Z]')
    def inspection__RSLT_D1(self):
        if g.E('RSLT_D') and g.S('RSLT_D') not in app.td['V017']['IDDR']:
            return Error(self.RSLT_D, message='001F.00.0240')
        if g.NE('RSLT_D') and g.E('IDCASE'):
            return Error(self, message='001K.00.0480')
        if g.E('RSLT_D') and g.S('RSLT_D') not in app.td['V017']['IDDR'] and g.E('DS_ONK') and g.I('DS_ONK') == 1:
            if g.I('RSLT_D') == 1:
                return Error(self.RSLT_D, message='002K.00.0350')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__NPR_DATE(self):
        # (FOR_POM=3 и USL_OK=1) или USL_OK=2 003F.00.0410
        if g.E('FOR_POM') and g.E('USL_OK') and ((g.I('FOR_POM') == 3 and g.I('USL_OK') == 1) or g.I('USL_OK') == 2):
            if g.NE('NPR_DATE'):
                return Error(self.root.ZL_LIST.ZAP.Z_SL, message='003F.00.0410')
        if g.E('NPR_DATE') and g.E('DATE_Z_1'):
            try:
                date1 = g.D('NPR_DATE')
                date2 = g.D('DATE_Z_1')
            except Exception as e:
                return Error(self, message='004F.00.0540')
            if date1 > date2:
                return Error(self.NPR_DATE, message='002K.00.0310')
        if g.NE('NPR_DATE') and g.E('NPR_MO'):
            return Error(self, message='003F.00.0402')
        if g.E('NPR_DATE'):
            if g.NE('NPR_MO'):
                return Error(self.NPR_DATE, message='002K.00.0300')

    @applicable_docs('R, D\d')
    def inspection__P_DISP2(self):
        if g.E('NOVOR') and g.I('NOVOR') != 0:
            if g.NE('P_DISP2'):
                return Error(self, message='003F.00.0421')
        if g.E('NOVOR') and g.I('NOVOR') == 0:
            if g.NE('P_DISP2'):
                return Error(self, message='002K.00.0320')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_Z_1(self):
        if g.E('DATE_Z_1') and g.E('DATE_Z_2'):
            try:
                date1 = g.D('DATE_Z_1')
            except Exception as e:
                return Error(self, message='003F.00.2260')
            try:
                date2 = g.D('DATE_Z_2')
            except Exception as e:
                return Error(self, message='003F.00.2270')
            if date1 > date2:
                return Error(self, message='002K.00.0330')
        if g.E('root.ZL_LIST.ZAP.PACIENT.DR'):
            list = [1, 4, 5, 6, 7, 8, 14, 16, 17, 26, 39]
            dr = g.D('root.ZL_LIST.ZAP.PACIENT.DR')
            try:
                date = g.D('DATE_Z_1')
                delta = relativedelta(date, dr).years
                if g.E('root.ZL_LIST.ZAP.PACIENT.DOCTYPE') and g.I('root.ZL_LIST.ZAP.PACIENT.DOCTYPE') in list:
                    if delta < 14:
                        return Error(self.DR, message='003K.00.1030')
            except:
                    return Error(self, message='001K.00.0360')

        # L файл не содержит данных о случае,тут надо отдельно вызов совместной проверки использовать

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_Z_2(self):
        if g.E('root.ZL_LIST.DSCHET') and g.E('DATE_Z_2'):
            try:
                date1 = g.D('DATE_Z_2')
            except Exception as e:
                return Error(self, message='003F.00.2270')
            try:
                date2 = g.D('root.ZL_LIST.SCHET.DSCHET')
            except Exception as e:
                return Error(self, message='003F.00.2070')
            if date1 > date2:
                return Error(self, message='002K.00.0340')

    @applicable_docs('C, H, R, T, X, D\w')
    def inspection__SUMP(self):
        if g.E('SUMP') and g.E('SUMV') and g.E('SANK_IT') and g.E(
                'OPLATA'):  # исправить проверку, эта никогда не выполнится, т.к. данные float а не int
            if g.I('SUMP') != g.I('SUMV') - g.I('SANK_IT'):
                return Error(self.SUMP, message='003K.00.0240')
            if g.I('OPLATA') != 3:
                if g.I('SUMP') == g.I('SUMV') - g.I('SANK_IT') and g.I('SUMP') > 0:
                    if g.I('SANK_IT') <= 0:
                        return Error(self.SUMP, message='003K.00.0260')
            if g.I('OPLATA') == 2:
                if g.I('SUMP') != 0:
                    if g.I('SANK_IT') != g.I('SUMV'):
                        return Error(self.SUMP, message='003K.00.0270')
            if g.I('OPLATA') == 1:
                if g.I('SUMP') == g.I('SUMV'):
                    if g.I('SANK_IT') != 0:
                        return Error(self.SUMP, message='003K.00.0280')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__IDCASE2(self):
        if g.NE('IDCASE') and g.E('LPU'):  # define level xml
            return Error(self, message='003F.00.2210')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__USL_OK11(self):
        if g.NE('USL_OK') and g.E('IDCASE'):
            return Error(self, message='003F.00.2220')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__VIDPOM11(self):
        if g.NE('VIDPOM') and g.E('IDCASE'):
            return Error(self, message='003F.00.2230')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__FOR_POM11(self):
        if g.NE('FOR_POM') and g.E('IDCASE'):
            return Error(self, message='003F.00.2240')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__LPU11(self):
        if g.NE('LPU') and g.E('IDCASE'):
            return Error(self, message='003F.00.2250')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_Z_11(self):
        if g.NE('DATE_Z_1') and g.E('IDCASE'):
            return Error(self, message='003F.00.2260')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_Z_21(self):
        if g.NE('DATE_Z_2') and g.E('IDCASE'):
            return Error(self, message='003F.00.2270')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__RSLT11(self):
        if g.NE('RSLT') and g.E('IDCASE'):
            return Error(self, message='003F.00.2280')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__ISHOD11(self):
        if g.NE('ISHOD') and g.E('IDCASE'):
            return Error(self, message='003F.00.2290')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__SL11(self):
        if g.NE('SL') and g.E('IDCASE'):
            return Error(self, message='003F.00.2300')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__IDSP11(self):
        if g.NE('IDSP') and g.E('IDCASE'):
            return Error(self, message='003F.00.2310')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__SUMV11(self):
        if g.NE('SUMV') and g.E('IDCASE'):
            return Error(self, message='003F.00.2320')

    @applicable_docs('T')
    def inspection__KD_Z11(self):
        if g.NE('KD_Z'):
            return Error(self, message='003F.00.2350')

    @applicable_docs('X, D[A-Z]')
    def inspection__VBR11(self):
        if g.NE('VBR') and g.E('IDCASE'):
            return Error(self, message='003F.00.2360')

    @applicable_docs('X, D[A-Z]')
    def inspection__P_OTK11(self):
        if g.NE('P_OTK') and g.E('IDCASE'):
            return Error(self, message='003F.00.2370')

    @applicable_docs('X, D[A-Z]')
    def inspection__RSLT_D11(self):
        if g.E('P_OTK'):
            if g.I('P_OTK') == 0:
                if g.NE('RSLT_D'):
                    return Error(self, message='003F.00.2381')
            if g.I('P_OTK') == 1:
                if g.E('RSLT_D'):
                    return Error(self, message='003F.00.2382')

    @applicable_docs('A')
    def inspection__OPLATA12(self):
        if g.NE('OPLATA') and g.E('IDCASE'):
            return Error(self, message='003F.00.3110')

    @applicable_docs('A')
    def inspection__SUMP12(self):
        if g.NE('SUMP') and g.E('IDCASE'):
            return Error(self, message='003F.00.3120')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__OS_SLUCH1(self):
        if g.E('OS_SLUCH'):
            check = re.fullmatch('^\d$', g.S('OS_SLUCH'))
            if not check:
                return Error(self.OS_SLUCH, message='004F.00.0610')


class SL(AbsModel):
    # todo: ?
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SUM_M(self):
        if g.E('SUM_M'):
            g.sum_m += g.F('SUM_M')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__DET(self):
        if g.E('DET') and g.E('root.ZL_LIST.Z_SL.DATE_Z_1') and g.E('PACIENT.DR'):
            date1 = g.D('root.ZL_LIST.Z_SL.DATE_Z_1')
            date2 = g.D('PACIENT.DR')
            delta = relativedelta(date1, date2).years
            if delta >= 18:
                return Error(self.DET, message='002K.00.0395')

    @applicable_docs('C, H, R, D\d')
    def inspection__DN(self):
        if g.E('P_CEL') and (g.S('P_CEL') == '1.3'):
            if g.NE('DN'):
                return Error(self, message='003F.00.0790')
        if g.E('DN'):
            if g.I('DN') > 9 or g.I('DN') < 0:
                return Error(self.DN, message='004F.00.0870')
            if g.I('DN') not in [1, 2, 4, 6]:
                return Error(self.DN, message='002F.00.0280')

    @applicable_docs('H, C, R, D\d')
    def inspection__KD(self):
        if g.E('USL_OK') and g.I('USL_OK') in [1, 2]:
            if g.NE('KD'):
                return Error(self, message='003F.00.0740')
        if g.E('KD') and g.E('USL_OK') and g.E('DATE_1') and g.E('DATE_2'):
            if g.E('KD') and g.I('USL_OK') in [1, 2]:
                date1 = g.D('DATE_1')
                date2 = g.D('DATE_2')
                delta = (date2 - date1).days
                if g.I('USL_OK') == 2:  # Дневной стационар
                    delta = delta + 1
                if g.I('KD') == 0 or g.I('KD') > delta:
                    return Error(self.KD, message='003K.00.0430')

    @applicable_docs('C, H, R, D\d')
    def inspection__KSG_KPG(self):
        if g.E('root.ZL_LIST.Z_SL.IDSP'):
            if g.I('root.ZL_LIST.Z_SL.IDSP') == 33 and g.NE('KSG_KPG'):
                return Error(self.KSG_KPG, message='003F.00.0800')
            if g.I('root.ZL_LIST.Z_SL.IDSP') != 33 and g.E('KSG_KPG'):
                return Error(self.KSG_KPG, message='003F.00.0810')

    @applicable_docs('T, R, D\d')
    def inspection__VID_HMP(self):
        if g.E('VID_HMP') and g.S('VID_HMP') not in app.td['V019']['HVID']:
            return Error(self.VID_HMP, message='003K.00.0530')

    @applicable_docs('T, R, D\d')
    def inspection__METOD_HMP(self):
        if g.E('METOD_HMP'):
            if g.S('METOD_HMP') not in app.td['V019']['IDHM']:
                return Error(self.METOD_HMP, message='003K.00.0540')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__PROFIL(self):
        if g.E('PROFIL') and g.E('DR') and g.E('W'):
            lst = [17, 18, 19, 20, 21, 68, 86]
            date1 = datetime.now()
            date2 = g.D('DR')
            delta = relativedelta(date1, date2)
            if g.S('PROFIL') in lst:
                if delta.years >= 18:
                    return Error(self.PROFIL, message='003K.00.0730')
            elif g.S('PROFIL') in [55]:
                if delta >= 1:
                    return Error(self.PROFIL, message='003K.00.0750')
            elif g.S('PROFIL') in [14]:
                if delta < 60:
                    return Error(self.PROFIL, message='003K.00.0760')
            elif g.S('PROFIL') in [29, 60, 108, 122]:
                if delta < 18:
                    return Error(self.PROFIL, message='003K.00.0770')
            if g.S('PROFIL') in [3, 136, 137, 184]:
                if g.I('W') != 2:
                    return Error(self.PROFIL, message='003K.00.0740')

    @applicable_docs('H, C, R, D\d')
    def inspection__PROFIL_K_1(self):
        if g.E('PROFIL_K') and g.E('root.ZL_LIST.ZAP.Z_SL.USL_OK'):
            try:
                if g.I('root.ZL_LIST.ZAP.Z_SL.USL_OK') in [1, 2]:
                    if g.S('PROFIL_K') not in app.td['V020']['IDK_PR']:
                        return Error(self, message='001F.00.0280')
                else:
                    return Error(self, message='003F.00.0650')
            except Exception as e:
                return Error(self, message='003F.00.0640')

    @applicable_docs('T')
    def inspection__PROFIL_K_2(self):
        if g.E('PROFIL_K'):
            try:
                if g.S('PROFIL_K') not in app.td['V020']['IDK_PR']:
                    return Error(self, message='001K.00.0630')
            except Exception as e:
                return Error(self, message='003F.00.2530')

    @applicable_docs('C, R,  H, D\d')
    def inspection__P_CEL(self):
        if g.E('P_CEL'):
            if g.S('P_CEL') not in app.td['V025']['IDPC']:
                return Error(self.P_CEL, message='001F.00.0290')
            if g.E('root.ZL_LIST.ZAP.Z_SL.USL_OK') and g.I('root.ZL_LIST.ZAP.Z_SL.USL_OK') != 3:
                return Error(self.P_CEL, message='003F.00.0660')
        if g.E('root.ZL_LIST.ZAP.Z_SL.USL_OK') and g.I('root.ZL_LIST.ZAP.Z_SL.USL_OK') == 3:
            if g.NE('P_CEL'):
                return Error(self, message='003F.00.0670')

    @applicable_docs('T')
    def inspection__C_ZAB2(self):
        if g.S('DS1') < 'D01' and g.S('DS1') >= 'C00.0':
            if g.NE('C_ZAB'):
                return Error(self, message='003F.00.0771')

    @applicable_docs('C, D\d, R')
    def inspection__C_ZAB3(self):
        if g.E('USL_OK'):
            if (g.S('DS1') < 'D01' and g.S('DS1') >= 'C00.0') and g.I('USL_OK') != 4:
                if g.NE('C_ZAB'):
                    return Error(self, message='003F.00.0781')

    @applicable_docs('H, C, R,  T, D\d')
    def inspection__C_ZAB(self):
        if g.E('C_ZAB') and g.S('C_ZAB') not in app.td['V027']['IDCZ']:
            return Error(self.C_ZAB, message='001F.00.0300')

    # @applicable_docs('H, T, C, R, X, D\w')
    @applicable_docs('R,D\d')
    def inspection__PRVS(self):
        list1 = [11, 12, 13, 31, 32, 33]
        list3 = [12, 13, 31]
        list2 = [42, 67, 82, 83, 1, 7, 14, 16, 29, 30, 34, 42, 51, 52, 53, 54, 63, 64, 67, 73, 74, 82, 83, 85, 86, 88,
                 96, 97, 98, 99, 234, 280]
        list4 = [100, 101, 206, 207, 208, 209, 210, 213, 217, 219, 221, 223, 224, 226, 227, 228, 230, 231, 233, 281,
                 284]
        list5 = [21, 22, 23]
        list6 = [66, 283, 4, 49]
        if g.E('PRVS') and g.E('VIDPOM'):
            if g.I('VIDPOM') in list1:
                if g.I('PRVS') not in list2:
                    return Error(self.PRVS, message='003K.00.0490')
            if g.I('VIDPOM') in list3:
                if g.I('PRVS') not in list4:
                    return Error(self.PRVS, message='003K.00.0500')
            if g.I('VIDPOM') in list5:
                if g.I('PRVS') not in list6:
                    return Error(self.PRVS, message='003K.00.0510')
        if g.E('PRVS'):
            if g.E('root.ZL_LIST.ZAP.PACIENT.DR'):
                dr = g.D('root.ZL_LIST.ZAP.PACIENT.DR')
                if g.E('DATE_1'):
                    date = g.D('DATE_1')
                    delta = relativedelta(date, dr).years
                    list7 = [18, 19, 20, 21, 22, 49, 68]
                    list8 = [25, 41, 84, 92]
                    if g.I('PRVS') in list7:
                        if delta >= 18:
                            return Error(self.PRVS, message='003K.00.0830')
                    if g.I('PRVS') == 2:
                        if g.I('root.ZL_LIST.ZAP.PACIENT.W') != 2:
                            return Error(self.PRVS, message='003K.00.0840')
                    if g.I('PRVS') == 37:
                        if delta >= 1:
                            return Error(self.PRVS, message='003K.00.0850')
                    if g.I('PRVS') in list8:
                        if delta < 18:
                            return Error(self.PRVS, message='003K.00.0860')
                    if g.I('PRVS') == 76:
                        if delta < 15:
                            return Error(self.PRVS, message='003K.00.0870')
                    if g.I('PRVS') == 11:
                        if delta < 60:
                            return Error(self.PRVS, message='003K.00.0880')
                else:
                    # return Error(self, message='001K.00.0360')
                    pass
            else:
                pass
                # return Error(self, message='001K.00.0300')

    @applicable_docs('H, C, R, D\d')
    def inspection__REAB(self):
        if g.E('NOVOR') and g.I('NOVOR') == 0:
            if g.NE('REAB'):
                return Error(self, message='002K.00.0390')

    @applicable_docs('R, D\d')
    def inspection__VID_HMP(self):
        if g.E('VID_HMP') and g.E('KSG_KPG') and g.NE('METHOD_HMP'):
            return Error(self.VID_HMP, message='OUCH_VID_HMP')

    @applicable_docs('R, D\d')
    def inspection__METHOD_HMP(self):
        if g.E('METHOD_HMP') and g.E('KSG_KPG') and g.NE('VID_HMP'):
            return Error(self.METHOD_HMP, message='OUCH_METHOD_HMP')

    @applicable_docs('T')
    def inspection__PROFIL_K_3(self):
        if g.NE('PROFIL_K'):
            return Error(self, message='001K.00.0630')

    @applicable_docs('H, T, C, R, D/w, X')
    def inspection__DATE_11(self):
        if g.E('DATE_1'):
            if g.E('root.ZL_LIST.Z_SL.DATE_Z_1'):
                date1 = g.D('root.ZL_LIST.Z_SL.DATE_Z_1')
                date2 = g.D('DATE_1')
                if date1 > date2:
                    return Error(self.DATE_1, message='002K.00.0400')
            if g.E('root.ZL_LIST.Z_SL.DATE_Z_2'):
                date1 = g.D('root.ZL_LIST.Z_SL.DATE_Z_2')
                date2 = g.D('DATE_1')
                if date1 > date2:
                    return Error(self.DATE_1, message='002K.00.0410')

    @applicable_docs('H, T, D\w, C, R, X')
    def inspection__DATE_12(self):
        if g.NE('DATE_1'):
            return Error(self, message='003F.00.2430')
        if g.NE('DATE_2'):
            return Error(self, message='003F.00.2440')
        if g.E('DATE_1'):
            if g.E('DATE_2'):
                try:
                    date1 = g.D('DATE_1')
                except Exception as e:
                    return Error(self, message='003F.00.2430')
                try:
                    date2 = g.D('DATE_2')
                except Exception as e:
                    return Error(self, message='003F.00.2440')
                if date1 > date2:
                    return Error(self.DATE_1, message='006F.00.0410')
            if g.E('root.ZL_LIST.Z_SL.DATE_Z_1'):
                try:
                    date1 = g.D('DATE_1')
                    date2 = g.D('root.ZL_LIST.Z_SL.DATE_Z_1')
                    if date1 < date2:
                        return Error(self.DATE_1, message='006F.00.0400')
                except Exception as e:
                    return Error(self, message='006F.00.0400')

    @applicable_docs('H, T, D\w, C, R, X')
    def inspection__DATE_21(self):
        if g.E('DATE_2') and g.E('root.ZL_LIST.Z_SL.DATE_Z_2'):
            date1 = g.D('DATE_2')
            date2 = g.D('root.ZL_LIST.Z_SL.DATE_Z_2')
            if date1 > date2:
                return Error(self.DATE_2, message='006F.00.0420')
            if date1 > date2:
                return Error(self.DATE_2, message='002K.00.0420')

    # @applicable_docs('H, T, DP, DV, DO, DS, DU, DF, C, R, D')
    # def inspection__DATE_22(self):
    #     if g.E('DATE_2') and g.E('DATE_1'):
    #         date1 = g.D('DATE_2')
    #         date2 = g.D('DATE_1')
    #         if date1 < date2:
    #             return Error(self.DATE_2, message='OUCH_DATE_22')

    @applicable_docs('H, T, C, R, D\w, X')
    def inspection__DS1(self):
        if g.E('DS1') and g.E('DS2') and g.E('DS3'):
            if g.S('DS1') == g.S('DS2') or g.S('DS1') == g.S('DS3'):
                return Error(self.DS1, message='006F.00.0430')
            if g.S('DS1') == g.S('DS2') or g.S('DS1') == g.S('DS3'):
                return Error(self.DS1, message='002K.00.0430')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__DS2(self):
        if g.E('DS2') and g.E('DS1') and g.E('DS3'):
            if g.S('DS2') == g.S('DS1') or g.S('DS2') == g.S('DS3'):
                return Error(self.DS2, message='002K.00.0440')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__CODE_MES1(self):
        if g.E('CODE_MES1') and g.E('CODE_MES2'):
            if g.S('CODE_MES1') == g.S('CODE_MES2'):
                return Error(self.CODE_MES1, message='002K.00.0450')

    @applicable_docs('H, T, C, R, D\w, X')
    def inspection__TARIF(self):
        if g.E('TARIF') and g.E('USL_OK') and g.E('SUM_M'):
            if g.E('TARIF') and g.I('USL_OK') in [1, 2]:
                if g.I('TARIF') > g.I('SUM_M'):
                    return Error(self.TARIF, message='002K.00.0460')

    @applicable_docs('H, C, R, D\d')
    def inspection__ED_COL1(self):
        if g.E('ED_COL') and g.E('USL_OK') and g.E('IDSP'):
            if g.I('USL_OK') == 3 and g.I('IDSP') == 28:
                if g.I('ED_COL') != 0:
                    return Error(self.ED_COL, message='003K.00.0560')

    @applicable_docs('H, C, R, T, D\d')
    def inspection__ED_COL2(self):
        if g.E('ED_COL') and g.E('USL_OK') and g.E('IDSP'):
            if g.I('USL_OK') == 3 and g.I('IDSP') in [25, 29]:
                if g.I('ED_COL') != 0:
                    return Error(self.ED_COL, message='003K.00.0550')

    @applicable_docs('C, D\d, R')
    def inspection__USL2(self):
        if g.E('USL_TIP'):
            if ('C00' <= g.S('DS1') < 'D10') or (
                    g.S('DS1') == 'D70' and g.E('DS2') and g.S('DS2') is not None and (
                    'C00' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')) and g.I(
                'USL_TIP') in [1, 3, 4, 6]:
                if g.NE('USL'):
                    return Error(self, message='003F.00.0960')

    @applicable_docs('T')
    def inspection__CONS(self):
        if g.E('DS_ONK'):
            if (g.I('DS_ONK') == 1) or ('C00.0' <= g.S('DS1') < 'D10'):
                if g.NE('CONS'):
                    return Error(self, message='003F.00.0831')
            if g.I('DS_ONK') == 0 and (('C00.0' > g.S('DS1')) or (g.S('DS1') > 'D09')):
                if g.E('CONS'):
                    return Error(self, message='003F.00.0851')

    @applicable_docs('D\d, R')
    def inspection__CONS2(self):
        if g.E('DS_ONK') and g.E('DISP'):
            if (g.I('DS_ONK') == 1) or ('C00.0' <= g.S('DS1') < 'D10') and g.I('DISP') == 0:
                if g.NE('CONS'):
                    return Error(self, message='003F.00.0832')
            if g.I('DISP') == 1 or (g.I('DS_ONK') == 0 and ('C00.0' > g.S('DS1') or g.S('DS1') > 'D09')):
                if g.E('CONS'):
                    return Error(self, message='003F.00.0852')

    @applicable_docs('C')
    def inspection__CONS3(self):
        #   if ('C00.0' <= g.S('DS1') < 'D10') or (
        #           g.S('DS1') == 'D70' and g.E('DS2') and (g.S('DS2') is not None) and (
        #           'C00.0' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')):
        #       if g.NE('CONS'):
        #           return Error(self, message='003F.00.0840')   #  old
        if ('C00.0' <= g.S('DS1') < 'D10'):
            if g.NE('CONS'):
                return Error(self, message='003F.00.0841')

    @applicable_docs('T')
    def inspection__ONK_SL(self):
        if ('C00.0' <= g.S('DS1') < 'D10'):
            if g.NE('ONK_SL'):
                return Error(self, message='003F.00.0871')
        if (g.S('DS1') < 'C00' or g.S('DS1') > 'D09') and (g.S('DS1') != 'D70') and (
                g.E('DS2') and g.S('DS2') is not None and ('C00.0' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')):
            if g.NE('ONK_SL'):
                return Error(self, message='003F.00.0900')
        if (g.S('DS1') < 'C00' or g.S('DS1') > 'D09'):
            if g.E('ONK_SL'):
                return Error(self, message='003F.00.0901')

    @applicable_docs('C')
    def inspection__ONK_SL2(self):
        if g.E('USL_OK') and g.E('REAB') and g.E('DS_ONK'):
            if ('C00.0' <= g.S('DS1') < 'D10') and g.I('USL_OK') != 4 and g.I('REAB') != 1 and g.I('DS_ONK') == 0:
                if g.NE('ONK_SL'):
                    return Error(self, message='003F.00.0881')
            if ((g.S('DS1') < 'C00' or g.S('DS1') > 'D09') and (
                    g.S('DS1') != 'D70' and g.E('DS2') and (g.S('DS2') is not None) and (
                    'C00.0' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97'))) or g.I(
                'USL_OK') == 4 or g.I('REAB') == 1:
                if g.NE('ONK_SL'):
                    return Error(self, message='003F.00.0910')
            if g.S('DS1') < 'C00' or g.S('DS1') > 'D09' or g.S('DS1') or g.I('USL_OK') == 4 or g.I('REAB') == 1:
                if g.NE('ONK_SL'):
                    return Error(self, message='003F.00.0911')

    @applicable_docs('D/d, R')
    def inspection__ONK_SL3(self):
        if g.E('USL_OK') and g.E('REAB') and g.E('DS_ONK') and g.E('DISP'):
            if ('C00.0' <= g.S('DS1') < 'D10') or (
                    g.S('DS1') == 'D70' and g.E('DS2') and (g.S('DS2') != '' and g.S('DS2') is not None) and
                    ('C00.0' <= g.S('DS2') < 'C81' or g.S('DS1') == 'C97')) and (
                    g.I('USL_OK') != 4 and g.I('REAB') != 1 and g.I('DS_ONK') == 0) and g.I('DISP') != 1:
                if g.NE('ONK_SL'):
                    return Error(self, message='003F.00.0890')
            if ('C00.0' <= g.S('DS1') < 'D10') and (
                    g.I('USL_OK') != 4 and g.I('REAB') != 1 and g.I('DS_ONK') == 0) and g.I('DISP') != 1:
                if g.NE('ONK_SL'):
                    return Error(self, message='003F.00.0891')
            if ((g.S('DS1') < 'C00' or g.S('DS1') > 'D09') and (
                    g.S('DS1') != 'D70' and g.E('DS2') and (g.S('DS2') != '' and g.S('DS2') is not None) and
                    ('C00.0' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97'))) or g.I(
                'USL_OK') == 4 or g.I('REAB') == 1 or g.I('DISP') == 1:
                if g.NE('ONK_SL'):
                    return Error(self, message='003F.00.0920')
            if (g.S('DS1') < 'C00' or g.S('DS1') > 'D09') or g.I(
                    'USL_OK') == 4 or g.I('REAB') == 1 or g.I('DISP') == 1:
                if g.NE('ONK_SL'):
                    return Error(self, message='003F.00.0921')

    @applicable_docs('C, T')
    def inspection__TARIF2(self):
        '''if ('C00.0' <= g.S('DS1') < 'D10') or (
                g.S('DS1') == 'D70' and g.E('DS2') and (g.S('DS2') != '' and g.S('DS2') is not None) and
                ('C00.0' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')):
            if g.NE('TARIF'):
                return Error(self, message='003F.00.0930')'''

        if ('C00.0' <= g.S('DS1') < 'D10'):
            if g.NE('TARIF'):
                return Error(self, message='003F.00.0931')

    @applicable_docs('T')
    def inspection__USL3(self):
        if g.E('USL_TIP'):
            '''if ('C00.0' <= g.S('DS1') < 'D10') or (
                    g.S('DS1') == 'D70' and g.E('DS2') and (g.S('DS2') != '' and g.S('DS2') is not None) and
                    ('C00.0' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')) and g.I(
                'USL_TIP') in [1, 3, 4]:
                if g.NE('USL'):
                    return Error(self, message='003F.00.0940')'''

            if ('C00.0' <= g.S('DS1') < 'D10') and g.I('USL_TIP') in [1, 3, 4]:
                if g.NE('USL'):
                    return Error(self, message='003F.00.0941')

    @applicable_docs('X')
    def inspection__USL4(self):
        if g.E('P_OTK') and g.I('P_OTK') == 1:
            if g.NE('USL'):
                return Error(self, message='003F.00.0950')

    @applicable_docs('C, D/d, R')
    def inspection__USL5(self):
        if g.E('USL_TIP'):
            if ('C00.0' <= g.S('DS1') < 'D10') or (
                    g.S('DS1') == 'D70' and g.E('DS2') and (g.S('DS2') != '' and g.S('DS2') is not None) and
                    ('C00.0' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')) and g.I(
                'USL_TIP') in [1, 3, 4, 6]:
                if g.NE('USL'):
                    return Error(self, message='003F.00.0960')
            if ('C00.0' <= g.S('DS1') < 'D10') and g.I('USL_TIP') in [1, 3, 4, 6]:
                if g.NE('USL'):
                    return Error(self, message='003F.00.0961')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__SL_ID11(self):
        if g.NE('SL_ID'):
            return Error(self, message='003F.00.2390')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__PROFIL11(self):
        if g.NE('PROFIL'):
            return Error(self, message='003F.00.2400')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__DET11(self):
        if g.NE('DET'):
            return Error(self, message='003F.00.2410')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__NHISTORY11(self):
        if g.NE('NHISTORY'):
            return Error(self, message='003F.00.2420')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_111(self):
        if g.NE('DATE_1'):
            return Error(self, message='003F.00.2430')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_211(self):
        if g.NE('DATE_2'):
            return Error(self, message='003F.00.2440')

    @applicable_docs('C, D, H, R, T, D\d')
    def inspection__DS111(self):
        if g.NE('DS1'):
            return Error(self, message='003F.00.2451')

    @applicable_docs('X, D[A-Z]')
    def inspection__DS112(self):
        if g.E('P_OTK'):
            if g.I('P_OTK') == 0:
                if g.NE('DS1'):
                    return Error(self, message='003F.00.2451')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__PRVS11(self):
        if g.NE('PRVS'):
            return Error(self, message='003F.00.2460')

    @applicable_docs('C, H, R, T, D\d')
    def inspection__VERS_SPEC11(self):
        if g.NE('VERS_SPEC'):
            return Error(self, message='003F.00.2470')

    @applicable_docs('C, H, T')
    def inspection__IDDOKT11(self):
        if g.NE('IDDOKT'):
            return Error(self, message='003F.00.2480')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__SUM_M11(self):
        if g.NE('SUM_M'):
            return Error(self, message='003F.00.2490')

    @applicable_docs('C, D, R, T, D\d')
    def inspection__DS_ONK11(self):
        if g.NE('DS_ONK'):
            return Error(self, message='003F.00.2500')

    @applicable_docs('T')
    def inspection__VID_HMP11(self):
        if g.NE('VID_HMP'):
            return Error(self, message='003F.00.2510')

    @applicable_docs('T')
    def inspection__METOD_HMP11(self):
        if g.NE('METOD_HMP'):
            return Error(self, message='003F.00.2520')

    @applicable_docs('T')
    def inspection__PROFIL_K11(self):
        if g.NE('PROFIL_K'):
            return Error(self, message='003F.00.2530')

    @applicable_docs('T')
    def inspection__TAL_D11(self):
        if g.NE('TAL_D'):
            return Error(self, message='003F.00.2540')

    @applicable_docs('T')
    def inspection__TAL_NUM11(self):
        if g.NE('TAL_NUM'):
            return Error(self, message='003F.00.2550')

    @applicable_docs('T')
    def inspection__TAL_P11(self):
        if g.NE('TAL_P'):
            return Error(self, message='003F.00.2560')

    @applicable_docs('X, D[A-Z]')
    def inspection__PR_D_N11(self):
        if g.E('P_OTK'):
            if g.I('P_OTK') == 0 or g.E('DS1'):
                if g.NE('PR_D_N'):
                    return Error(self, message='003F.00.2571')


class DS2_N(AbsModel):
    @applicable_docs('X, D[A-Z]')
    def inspection__DS2(self):
        if g.NE('DS2'):
            return Error(self, message='001K.00.1120')

    @applicable_docs('X, D[A-Z]')
    def inspection__PR_DS2_N(self):
        if g.NE('PR_DS2_N'):
            return Error(self, message='001K.00.1130')

    @applicable_docs('X, D[A-Z]')
    def inspection__DS221(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.DS2_N'):
            if g.NE('DS2'):
                return Error(self, message='003F.00.3020')

    @applicable_docs('X, D[A-Z]')
    def inspection__PR_DS2_N21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.DS2_N'):
            if g.NE('PR_DS2_N'):
                return Error(self, message='003F.00.3030')


class IDDOKT(AbsModel):
    pass


class NHISTORY(AbsModel):
    pass


class NAZ(AbsModel):

    @applicable_docs('D[A-Z], X')
    def inspection__NAZ_SP(self):
        if g.E('NAZ_SP'):
            if g.S('NAZ_SP') not in app.td['V021']['IDSPEC']:
                return Error(self.NAZ_SP, message='001F.00.0320')
        if g.E('NAZ_R'):
            if g.I('NAZ_R') in [1, 2]:
                if g.NE('NAZ_SP'):
                    return Error(self, message='003F.00.1270')
            if g.I('NAZ_R') not in [1, 2]:
                if g.E('NAZ_SP'):
                    return Error(self, message='003F.00.1280')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_PMP(self):
        if g.E('NAZ_PMP'):
            if g.S('NAZ_PMP') not in app.td['V002']['IDPR']:
                return Error(self.NAZ_PMP, message='001F.00.0330')
        if g.E('NAZ_R'):
            if g.I('NAZ_R') in [4, 5]:
                if g.NE('NAZ_PMP'):
                    return Error(self.N, message='003F.00.1370')
            if g.I('NAZ_R') not in [4, 5]:
                if g.E('NAZ_PMP'):
                    return Error(self, message='003F.00.1380')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_PK(self):
        if g.E('NAZ_PK'):
            if g.S('NAZ_PK') not in app.td['V020']['IDK_PR']:
                return Error(self.NAZ_PMP, message='001F.00.0340')
        if g.E('NAZ_R'):
            if g.I('NAZ_R') == 6:
                if g.NE('NAZ_PK'):
                    return Error(self, message='003F.00.1390')
            if g.I('NAZ_R') != 6:
                if g.E('NAZ_PK'):
                    return Error(self, message='003F.00.1400')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_V(self):
        if g.E('NAZ_V'):
            if g.S('NAZ_V') not in app.td['V029']['IDMET']:
                return Error(self.NAZ_V, message='001F.00.0350')
        if g.E('NAZ_R'):
            if g.I('NAZ_R') == 3:
                if g.NE('NAZ_V'):
                    return Error(self, message='003F.00.1290')
            if g.I('NAZ_R') != 3:
                if g.E('NAZ_V'):
                    return Error(self, message='003F.00.1300')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAPR_MO(self):
        if g.E('NAPR_MO'):
            if g.S('NAPR_MO') not in app.td['F003']['mcod']:
                return Error(self.NAPR_MO, message='001F.00.0360')
            if g.E('NAZ_R') and g.E('DS_ONK') and g.I('NAZ_R') not in [2, 3] or g.I('DS_ONK') != 1:
                return Error(self.NAPR_MO, message='003F.00.1350')

    # @applicable_docs('C, D\d, R, T')
    # def inspection__NAPR_MO2(self):
    #     if g.E('CODE_MO'):
    #         if g.I('NAPR_MO')==g.I('CODE_MO'):
    #             if

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_N(self):
        if g.NE('NAZ_N'):
            return Error(self, message='001K.00.1140')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_R(self):
        if g.NE('NAZ_R'):
            return Error(self.NAZ_R, message='001K.00.1150')
        if g.E('root.ZL_LIST.Z_SL.RSLT_D') and (g.I('root.ZL_LIST.Z_SL.RSLT_D') in [3, 4, 5, 31, 32]):
            try:
                if g.NE('NAZ_R') or g.I('NAZ_R') < 1:
                    return Error(self.NAZ_R,
                                 message='003K.00.0040')  # //////////////////////////////////////////////////
            except Exception as e:
                return Error(self.NAZ_R, message='001K.00.1150')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_USL(self):
        if g.E('NAZ_R') and g.E('DS_ONK'):
            if g.I('NAZ_R') == 3 and g.I('DS_ONK') == 1:
                if g.NE('NAZ_USL'):
                    return Error(self, message='003F.00.1310')
            if g.I('NAZ_R') != 3 or g.I('DS_ONK') == 0:
                if g.E('NAZ_USL'):
                    return Error(self, message='003F.00.1320')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAPR_DATE(self):
        if g.E('NAZ_R') and g.E('DS_ONK'):
            if g.I('NAZ_R') in [2, 3] and g.I('DS_ONK') == 1:
                if g.NE('NAPR_DATE'):
                    return Error(self, message='003F.00.1330')
            if g.I('NAZ_R') not in [2, 3] or g.I('DS_ONK') == 0:
                if g.E('NAPR_DATE'):
                    return Error(self, message='003F.00.1340')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAPR_MO(self):
        if g.E('NAZ_R') and g.E('DS_ONK'):
            if g.I('NAZ_R') in [2, 3] and g.I('DS_ONK') == 1:
                if g.NE('NAPR_MO'):
                    return Error(self, message='003F.00.1350')
            if g.I('NAZ_R') not in [2, 3] or g.I('DS_ONK') == 0:
                if g.E('NAPR_MO'):
                    return Error(self, message='003F.00.1360')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_N21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.NAZ'):
            if g.NE('NAZ_N'):
                return Error(self, message='003F.00.3040')

    @applicable_docs('X, D[A-Z]')
    def inspection__NAZ_R21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.NAZ') and g.E('RESL_D'):
            if g.I('RSLT_D') in [3, 4, 5, 31, 32]:
                if g.NE('NAZ_N'):
                    return Error(self, message='003F.00.3051')


class ONK_SL(AbsModel):

    @applicable_docs('T')
    def inspection__ONK_USL3(self):
        if g.NE('ONK_USL'):
            return Error(self, message='003F.00.2600')

    @applicable_docs('T, C, R, D\d')
    def inspection__DS1_T(self):
        if g.NE('DS1_T'):
            return Error(self, message='001K.00.0690')

    @applicable_docs('T')
    def inspection__STAD(self):
        if g.E('DS1_T') and g.I('DS1_T') in [0, 1, 2]:
            if g.NE('STAD'):
                return Error(self, message='003F.00.0970')

    @applicable_docs('C, D/d, R')
    def inspection__STAD2(self):
        if g.E('DS1_T') and g.E('DATE_Z_1'):
            if g.I('DS1_T') in [0, 1, 2, 3, 4]:
                if g.NE('STAD'):
                    return Error(self, message='003F.00.0980')

    @applicable_docs('R')
    def inspection__ONK_T(self):
        if g.E('DS1_T') and g.E('root.ZL_LIST.ZAP.Z_SL.DATE_Z_1'):   # пути к атрибутам нужны, на разных уровнях они
            dr = g.D('root.ZL_LIST.ZAP.PACIENT.DR')
            date = g.D('root.ZL_LIST.ZAP.Z_SL.DATE_Z_1')
            delta = relativedelta(date,dr).years
            if g.I('DS1_T') == 0 and delta >= 18:
                if g.NE('ONK_T'):
                    return Error(self, message='003F.00.0990')

    @applicable_docs('T, C, R, D\d')
    def inspection__ONK_N(self):
        if g.E('ONK_N') and g.S('ONK_N') not in app.td['N004']['ID_N']:
            return Error(self.ONK_N, message='ohONK_N(')
        if g.E('DS1_T') and g.E('DATE_Z_1'):
            dr = g.D('DR')
            date = g.D('DATE_Z_1')
            delta = relativedelta(date, dr).years
            if g.I('DS1_T') == 0 and delta >= 18:
                if g.NE('ONK_N'):
                    return Error(self, message='003F.00.1000')

    @applicable_docs('T, C, R, D\d')
    def inspection__ONK_M(self):
        if g.E('ONK_M') and g.S('ONK_M') not in app.td['N005']['ID_M']:
            return Error(self.ONK_M, message='ohONK_M')
        if g.E('DS1_T') and g.E('DATE_Z_1'):
            dr = g.D('DR')
            date = g.D('DATE_Z_1')
            delta = relativedelta(date, dr).years
            if g.I('DS1_T') == 0 and delta >= 18:
                if g.NE('ONK_M'):
                    return Error(self, message='003F.00.1010')

    @applicable_docs('T, C, R, D\d')
    def inspection__MTSTZ(self):
        if g.E('DS1_T') and not g.I('DS1_T') in [1, 2] and g.E('MTSTZ'):
            # if g.NE('MTSTZ'):
            return Error(self, message='003F.00.1020')

    # @applicable_docs('T, C, R, D\d')
    # def inspection__SOD(self):
    #     if g.NE('SOD'):
    #         return Error(self.SOD,
    #                      message='003F.00.1030')  # ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @applicable_docs('T, C, R, D\d')
    def inspection__WEI(self):
        if g.E('BSA') and (g.S('BSA') != '' and g.S('BSA') != ' ' and g.S('BSA') is not None):
            if g.NE('WEI'):
                return Error(self, message='003F.00.1070')

    @applicable_docs('T, C, R, D\d')
    def inspection__HEI(self):
        if g.E('BSA') and (g.S('BSA') != '' and g.S('BSA') != ' ' and g.S('BSA') is not None):
            if g.NE('HEI'):
                return Error(self, message='003F.00.1080')

    @applicable_docs('C, D\d, R')
    def inspection__ONK_USL2(self):
        if g.E('USL_OK') and g.E('DS1_T'):
            if g.I('USL_OK') in [1, 2] and g.I('DS1_T') in [0, 1, 2]:
                if g.NE('ONK_USL'):
                    return Error(self, message='003F.00.1090')

    @applicable_docs('C, D\d, R, T')
    def inspection__DS1_T11(self):
        if g.E('ONK_SL'):
            if g.NE('DS1_T'):
                return Error(self, message='003F.00.2590')

        # if g.E('ONK_M') and g.NE('ONK_SL'):
        #    return Error(self.ONK_USL, message='001K.00.0700')


class B_DIAG(AbsModel):

    @applicable_docs('T, C, R, D\d')
    def inspection__DIAG_CODE1(self):
        if g.E('DIAG_CODE') and g.E('DIAG_TIP'):
            if g.I('DIAG_TIP') == 1 and g.NE('DIAG_DATE'):
                if g.S('DIAG_CODE') not in app.td['N007']['ID_Mrf']:
                    return Error(self.DIAG_CODE, message='ohDIAG_CODE1')

    @applicable_docs('T, C, R, D\d')
    def inspection__DIAG_CODE4(self):
        if g.E('DIAG_CODE') and g.E('DIAG_TIP') and g.E('DIAG_TIP') and g.E('DS1'):
            if g.I('DIAG_TIP') == 1 and g.S('DIAG_TIP') not in app.td['N009']['ID_Mrf']:
                if g.S('DS1') not in app.td['N009']['ID_Mrf']:
                    return Error(self.DIAG_CODE, message='002K.00.0470')

    # g.s('') in ['1', '2', '3']
    @applicable_docs('T, C, R, D\d')
    def inspection__DIAG_CODE2(self):
        if g.E('DIAG_CODE') and g.E('DIAG_TIP') and g.E('DS1'):
            if g.I('DIAG_TIP') == 2 and g.S('DS1') in app.td['N012']:
                if g.S('DS1') in app.td['N010']['ID_Igh']:
                    return Error(self.DIAG_CODE, message='002K.00.0490')
        if g.NE('DIAG_CODE'):
            return Error(self, message='001K.00.0730')

    @applicable_docs('C, R, D\d')
    def inspection__DIAG_CODE3(self):
        if g.E('DIAG_CODE') and g.E('DIAG_TIP'):
            if g.I('DIAG_TIP') == 2 and g.NE('DIAG_DATE'):
                return Error(self.DIAG_CODE, message='OUCH_DIAG_CODE3')

    @applicable_docs('T, C, R, D\d')
    def inspection__DIAG_RSLT1(self):
        if g.E('DIAG_RSLT') and g.E('DIAG_TIP') and g.E('DIAG_CODE'):
            if g.I('DIAG_TIP') == 1:
                if g.S('DIAG_CODE') not in app.td['N008']['ID_Mrf']:
                    return Error(self.DIAG_RSLT, message='002K.00.0480')
        if g.E('REC_RSLT') and g.I('REC_RSLT') == 1:
            if g.NE('DIAG_RSLT'):
                return Error(self, message='003F.00.1250')

    @applicable_docs('T, C, R, D\d')
    def inspection__DIAG_RSLT2(self):
        if g.E('DIAG_RSLT') and g.E('DIAG_TIP') and g.E('DIAG_CODE'):
            if g.I('DIAG_TIP') == 2:
                if g.S('DIAG_CODE') not in app.td['N011']['ID_Igh']:
                    return Error(self.DIAG_RSLT, message='002K.00.0500')

    @applicable_docs(' C, R, D\d, T')
    def inspection__DIAG_TIP(self):
        if g.NE('DIAG_TIP'):
            return Error(self, message='001K.00.0720')

    # @applicable_docs('C, R, D')
    # def inspection__DIAG_RSLT3(self):
    #     if g.E('DIAG_RSLT') and (g.I('DIAG_TIP') == 1 and not g.E('DIAG_DATE')):
    #         return Error(self.DIAG_RSLT, message='OUCH_DIAG_RSLT3')

    @applicable_docs('C, R, D\d')
    def inspection__DIAG_DATE(self):
        if g.NE('DIAG_DATE'):
            return Error(self, message='001K.00.0710')

    @applicable_docs(' C, R, D\d, T')
    def inspection__REC_RSLT(self):
        if g.E('DIAG_RSLT'):
            if g.NE('REC_RSLT'):
                return Error(self, message='003F.00.1260')

    @applicable_docs('C, D\d, R, T')
    def inspection__DIAG_DATE11(self):
        if g.E('B_DIAG'):
            if g.NE('DIAG_DATE'):
                return Error(self, message='003F.00.2610')

    @applicable_docs('C, D\d, R, T')
    def inspection__DIAG_TIP11(self):
        if g.E('B_DIAG'):
            if g.NE('DIAG_TIP'):
                return Error(self, message='003F.00.2620')

    @applicable_docs('C, D\d, R, T')
    def inspection__DIAG_CODE11(self):
        if g.E('B_DIAG'):
            if g.NE('DIAG_CODE'):
                return Error(self, message='003F.00.2630')


class B_PROT(AbsModel):

    @applicable_docs('T, C, R, D\d')
    def inspection__PROT(self):
        if g.E('PROT') and g.NE('B_PROT'):
            return Error(self.PROT, message='001K.00.0740')
        if g.E('B_PROT'):
            if g.NE('PROT'):
                return Error(self, message='003F.00.2640')

    @applicable_docs('T, C, R, D\d')
    def inspection__D_PROT(self):
        if g.E('D_PROT') and g.E('root.ZL_LIST.SL.DATE_2'):
            date1 = g.D('D_PROT')
            date2 = g.D('root.ZL_LIST.SL.DATE_2')
            if date1 > date2:
                return Error(self.D_PROT, message='002K.00.0510')
            if g.NE('B_PROT'):
                return Error(self, message='001K.00.0750')

    @applicable_docs('C, D\d, R, T')
    def inspection__D_PROT11(self):
        if g.E('B_PROT'):
            if g.NE('D_PROT'):
                return Error(self, message='003F.00.2650')


class KSG_KPG(AbsModel):

    @applicable_docs('H, C, R, D\d')
    def inspection__N_KSG(self):
        if g.E('N_KSG') and g.E('KSG_PG') and g.E('IDSP'):
            if g.I('KSG_PG') == 0:
                # if all([g.I('KSG_PG') == 0, 33 <= g.I('IDSP') <= 43, not g.E('N_KPG')]):
                if g.S('N_KSG') not in app.td['V023']['K_KSG']:
                    return Error(self.N_KSG, message='001F.00.0580')
        if g.E('IDSP') and g.I('IDSP') and g.NE('N_KPG'):
            if g.NE('N_KSG'):
                return Error(self, message='003F.00.1100')
        if g.E('N_KPG'):
            if g.E('N_KSG'):
                return Error(self.N_KSG, message='003F.00.1110')

    @applicable_docs('C, R,  H, D\d')
    def inspection__N_KPG1(self):
        if g.E('N_KPG') and g.E('IDSP') and g.NE('N_KSG'):
            # if 33 <= g.I('IDSP') <= 43:
            if g.S('N_KPG') not in app.td['V026']['K_KPG']:
                return Error(self.N_KPG, message='001F.00.0590')
        if g.E('IDSP') and g.I('IDSP') and g.NE('N_KSG'):
            if g.NE('N_KPG'):
                return Error(self, message='003F.00.1120')

    @applicable_docs('C, R, D\d')
    def inspection__DKK2(self):
        if g.E('DKK2') and g.E('DS1') and g.E('DS2') and g.E('DKK2'):
            if 'C00' <= g.S('DS1') <= 'C97':
                if not g.S('DKK2') in app.td['V024']['IDDKK']:
                    return Error(self.DKK2, message='ohDKK2')
            elif g.S('DS1') == 'D70':
                if g.S('DS2') == 'C97' or 'C00' <= g.S('DS2') <= 'C80':
                    if not g.S('DKK2') in app.td['V024']['IDDKK']:
                        return Error(self.DKK2, message='ohDKK22')

    @applicable_docs('H, C, R, D\d')
    def inspection__N_KSG2(self):
        if g.E('N_KSG') and g.E('N_KPG'):
            return Error(self.N_KSG, message='003F.00.1110')

    @applicable_docs('H, C, R, D\d')
    def inspection__N_KPG2(self):
        if g.E('N_KPG') and g.E('N_KSG'):
            return Error(self.N_KPG, message='003F.00.1130')

    @applicable_docs('H, C, R, D\d')
    def inspection__IT_SL(self):
        if g.E('SL_KOEF') and g.NE('IT_SL'):
            return Error(self, message='003F.00.1140')
        if g.NE('SL_KOEF') and g.E('IT_SL'):
            return Error(self, message='003F.00.1150')
        if g.E('IT_SL') and g.NE('SL_KOEF'):
            return Error(self, message='003F.00.1160')
        if g.NE('IT_SL') and g.E('SL_KOEF'):
            return Error(self, message='003F.00.1170')
        if g.E('IT_SL') and g.E('SL_K'):
            try:
                if g.I('SL_K') not in [0, 1]:
                    return Error(self, message='002F.00.0450')
            except Exception as e:
                return Error(self, message='002F.00.0450')
            if g.I('SL_K') == 1:
                if g.F('IT_SL') == 1.00000 and g.E('root.ZL_LIST.ZAP.Z_SL.SL.PROFIL') and (
                        g.I('root.ZL_LIST.ZAP.Z_SL.SL.PROFIL') != 137):
                    return Error(self.IT_SL, message='003K.00.0970')

    @applicable_docs('H, C, R, D\d')
    def inspection__VER_KSG(self):
        if g.NE('VER_KSG'):
            return Error(self, message='001K.00.0790')

    @applicable_docs('H, C, R, D\d')
    def inspection__KSG_PG(self):
        if g.NE('KSG_PG'):
            return Error(self, message='001K.00.0800')

    @applicable_docs('H, C, R, D\d')
    def inspection__KOEF_Z(self):
        if g.NE('KOEF_Z'):
            return Error(self, message='001K.00.0810')

    @applicable_docs('H, C, R, D\d')
    def inspection__KOEF_UP(self):
        if g.NE('KOEF_UP'):
            return Error(self, message='001K.00.0820')

    @applicable_docs('H, C, R, D\d')
    def inspection__BZTSZ(self):
        if g.NE('BZTSZ'):
            return Error(self, message='001K.00.0830')

    @applicable_docs('H, C, R, D\d')
    def inspection__KOEF_D(self):
        if g.NE('KOEF_D'):
            return Error(self, message='001K.00.0840')

    @applicable_docs('H, C, R, D\d')
    def inspection__KOEF_U(self):
        if g.NE('KOEF_U'):
            return Error(self, message='001K.00.0850')

    @applicable_docs('H, C, R, D\d')
    def inspection__SL_K(self):
        if g.NE('SL_K'):
            return Error(self, message='001K.00.0860')

    @applicable_docs('C, R, T, D\d')
    def inspection__VER_KSG11(self):
        if g.E('KSG_KPG'):
            if g.NE('VER_KSG'):
                return Error(self, message='003F.00.2690')

    @applicable_docs('C, R, T, D\d')
    def inspection__KSP_PG11(self):
        if g.E('KSG_KPG'):
            if g.NE('KSG_PG'):
                return Error(self, message='003F.00.2700')

    @applicable_docs('C, R, T, D\d')
    def inspection__KOEF_Z11(self):
        if g.E('KSG_KPG'):
            if g.NE('KOEF_Z'):
                return Error(self, message='003F.00.2710')

    @applicable_docs('C, R, T, D\d')
    def inspection__KOEF_UP11(self):
        if g.E('KSG_KPG'):
            if g.NE('KOEF_UP'):
                return Error(self, message='003F.00.2720')

    @applicable_docs('C, R, T, D\d')
    def inspection__BZTSZ11(self):
        if g.E('KSG_KPG'):
            if g.NE('BZTSZ'):
                return Error(self, message='003F.00.2730')

    @applicable_docs('C, R, T, D\d')
    def inspection__KOEF_D11(self):
        if g.E('KSG_KPG'):
            if g.NE('KOEF_D'):
                return Error(self, message='003F.00.2740')

    @applicable_docs('C, R, T, D\d')
    def inspection__KOEF_U11(self):
        if g.E('KSG_KPG'):
            if g.NE('KOEF_U'):
                return Error(self, message='003F.00.2750')

    @applicable_docs('C, R, T, D\d')
    def inspection__SL_K11(self):
        if g.E('KSG_KPG'):
            if g.NE('SL_K'):
                return Error(self, message='003F.00.2760')


class SL_KOEF(AbsModel):
    @applicable_docs('H, C, R, D\d')
    def inspection__IDSL(self):
        if g.NE('IDSL'):
            return Error(self, message='001K.00.0870')

    @applicable_docs('H, C, R, D\d')
    def inspection__Z_SL1(self):
        if g.NE('Z_SL') and g.E('IDSL'):
            return Error(self, message='001K.00.0880')

    @applicable_docs('C, R, T, D\d')
    def inspection__IDSL11(self):
        if g.E('SL_KOEF'):
            if g.NE('IDSL'):
                return Error(self, message='003F.00.2770')

    @applicable_docs('C, R, T, D\d')
    def inspection__Z_SL12(self):
        if g.E('SL_KOEF'):
            if g.NE('Z_SL'):
                return Error(self, message='003F.00.2780')


class SANK(AbsModel):

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__S_TIP(self):
        if g.NE('S_TIP'):
            return Error(self, message='001K.00.0910')

    @applicable_docs('H, T, DP, DV, DO, DS, DU, DF, C, R, D\d')
    def inspection__S_OSN(self):
        if g.E('S_OSN') and g.E('S_SUM'):
            if g.E('S_OSN') and g.I('S_SUM') != 0:
                if g.S('S_OSN') not in app.td['F014']['Kod']:
                    return Error(self.S_OSN, message='001F.00.0680')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__CODE_EXP(self):
        if g.E('S_TIP'):
            if g.I('S_TIP') != 30:
                if g.NE('CODE_EXP'):
                    return Error(self, message='003F.00.1790')

    @applicable_docs('A')
    def inspection__CODE_EXP2(self):
        if g.E('S_TIP') and g.E('S_IST'):
            if g.I('S_TIP') != 30 and g.I('S_IST') != 2:
                if g.NE('CODE_EXP'):
                    return Error(self, message='003F.00.1800')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__S_CODE(self):
        if g.NE('S_CODE'):
            return Error(self, message='001K.00.0890')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__S_SUM(self):
        if g.E('S_SUM') and g.E('SANK.S_TIP'):
            if g.NE('SANK'):
                return Error(self.S_SUM, message='001K.00.0900')
            if g.I('SANK.S_TIP') in [1, 10, 11, 12]:
                g.s_sum += g.I('S_SUM')
            if 30 <= g.I('SANK.S_TIP') < 50:
                g.s_sum2 += g.I('S_SUM')
            if 20 <= g.I('SANK.S_TIP') < 30:
                g.s_sum_mee += g.I('S_SUM')
            if g.I('S_SUM') > g.s_sum_max:
                g.s_sum_max = g.I('S_SUM')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_ACT(self):
        if g.NE('DATE_ACT'):
            return Error(self, message='001K.00.0920')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__NUM_ACT(self):
        if g.NE('NUM_ACT'):
            return Error(self, message='001K.00.0930')

    @applicable_docs('A')
    def inspection__NUM_ACT2(self):
        if g.I('S_IST') == 2 and g.I('S_TIP') not in [1, 10, 11, 12]:
            if g.E('NUM_ACT'):
                return Error(self.NUM_ACT, message='003F.00.1760')

    @applicable_docs('A, C, D, H, R, T, X, D\w')
    def inspection__S_IST(self):
        if g.NE('S_IST'):
            return Error(self, message='001K.00.0940')

    @applicable_docs('A')
    def inspection__DATE_ACT2(self):
        if g.E('S_IST') and g.E('S_TIP'):
            if g.I('S_IST') == 2 and g.I('S_TIP') not in [1, 10, 11, 12]:
                if g.E('DATE_ACT'):
                    return Error(self.DATE_ACT, message='003F.00.1750')

    @applicable_docs('A, C, H, R, T, X, D\w')
    def inspection__SL_ID(self):
        if g.E('S_SUM'):
            if g.I('S_SUM') != 0:
                if g.NE('SL_ID'):
                    return Error(self, message='003F.00.1770')

    @applicable_docs('A, C, H, R, T, X, D\w')
    def inspection__S_OSN2(self):
        if g.E('S_SUM'):
            if g.I('S_SUM') != 0:
                if g.NE('S_OSN'):
                    return Error(self, message='003F.00.1780')

    @applicable_docs('A, C, R, T, D\w, H, X')
    def inspection__S_CODE11(self):
        if g.E('SANK'):
            if g.NE('S_CODE'):
                return Error(self, message='003F.00.2790')

    @applicable_docs('A, C, R, T, D\w, H, X')
    def inspection__S_SUM11(self):
        if g.E('SANK'):
            if g.NE('S_SUM'):
                return Error(self, message='003F.00.2800')

    @applicable_docs('A, C, R, T, D\w, H, X')
    def inspection__S_TIP11(self):
        if g.E('SANK'):
            if g.NE('S_TIP'):
                return Error(self, message='003F.00.2810')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__DATE_ACT11(self):
        if g.E('SANK'):
            if g.NE('DATE_ACT'):
                return Error(self, message='003F.00.2820')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__NUM_ACT11(self):
        if g.E('SANK'):
            if g.NE('NUM_ACT'):
                return Error(self, message='003F.00.2830')

    @applicable_docs('A, C, R, T, D\w, H, X')
    def inspection__S_IST11(self):
        if g.E('SANK'):
            if g.NE('S_IST'):
                return Error(self, message='003F.00.2840')


class USL(AbsModel):

    @applicable_docs('C, R, H, D, T, X, D\w')
    def inspection__LPU_3(self):
        if g.E('LPU') and g.S('LPU') not in app.td['F003']['mcod'] and lc.Check.ter != "Orel":
            return Error(self, message='001F.00.0620')
            # return Error(self, message='001F.00.0620+'+g.S('LPU') + g.S('root.ZL_LIST.ZAP.N_ZAP'))
        if g.NE('LPU') and g.E('IDSERV'):
            return Error(self, message='001K.00.0960')

    @applicable_docs('H, T, C, R, D\d')
    def inspection__PROFIL(self):
        if g.NE('PROFIL') and g.E('IDSERV'):
            return Error(self, message='001K.00.0970')
        if g.E('DR') and g.E('PROFIL') and g.E('W'):
            date1 = datetime.now()
            date2 = g.D('DR')
            delta = relativedelta(date1, date2).years
            if g.I('PROFIL') in [55]:
                if delta >= 1:
                    return Error(self.PROFIL, message='003K.00.0800')
            elif g.I('PROFIL') in [14]:
                if delta < 60:
                    return Error(self.PROFIL, message='003K.00.0810')
            elif g.I('PROFIL') in [29, 60, 108, 122]:
                if delta < 18:
                    return Error(self.PROFIL, message='003K.00.0820')
            elif g.I('PROFIL') in [17, 18, 19, 20, 21, 68, 86]:
                if delta >= 18:
                    return Error(self.PROFIL, message='003K.00.0780')
            if g.I('PROFIL') in [3, 136, 137, 184]:
                if g.I('W') != 2:
                    return Error(self.PROFIL, message='003K.00.0790')

    # @applicable_docs('C, D, H, R, T, X, D\w')
    @applicable_docs('D\d, R')
    def inspection__PRVS01(self):
        if g.E('PRVS') and g.E('VIDPOM'):
            list = [42, 67, 82, 83, 1, 7, 14, 16, 29, 30, 34, 42, 51, 52, 53, 54, 63, 64, 67, 73, 74, 82, 83, 85, 86,
                    88, 96, 97, 98, 99, 234, 280]
            list2 = [11, 12, 13, 31, 32, 33]
            if g.I('VIDPOM') in list2:
                if g.I('PRVS') in list:
                    return Error(self.PRVS, message='003K.00.0490')
        if g.E('PRVS'):
            if g.E('root.ZL_LIST.ZAP.PACIENT.DR'):
                dr = g.D('root.ZL_LIST.ZAP.PACIENT.DR')
                if g.E('root.ZL_LIST.ZAP.Z_SL.SL.DATE_1'):
                    date = g.D('root.ZL_LIST.ZAP.Z_SL.SL.DATE_1')

                    delta = relativedelta(date, dr).years
                    list7 = [18, 19, 20, 21, 22, 49, 68]
                    list8 = [25, 41, 84, 92]
                    if g.I('PRVS') in list7:
                        if delta >= 18:
                            return Error(self.PRVS, message='003K.00.0890')
                    if g.I('PRVS') == 2:
                        if g.I('root.ZL_LIST.ZAP.PACIENT.W') != 2:
                            return Error(self.PRVS, message='003K.00.0900')
                    if g.I('PRVS') == 37:
                        if delta >= 1:
                            return Error(self.PRVS, message='003K.00.0910')
                    if g.I('PRVS') in list8:
                        if delta < 18:
                            return Error(self.PRVS, message='003K.00.0920')
                    if g.I('PRVS') == 76:
                        if delta < 15:
                            return Error(self.PRVS, message='003K.00.0930')
                    if g.I('PRVS') == 11:
                        if delta < 60:
                            return Error(self.PRVS, message='003K.00.0940')
                else:
                    pass
                    # return Error(self, message='001K.00.0360')
            else:
                pass
                # return Error(self, message='001K.00.0300')

    @applicable_docs('C, D, H, R, T')
    def inspection__PRVS3(self):
        if g.E('PRVS') and g.E('P_OTK'):
            if g.I('P_OTK') == 0:
                if g.S('PRVS') not in app.td['V021']['IDSPEC']:
                    return Error(self.PRVS, message='001F.00.0662')

    @applicable_docs('D[A-Z]')
    def inspection__PRVS2(self):
        if g.E('PRVS') and g.S('PRVS') not in app.td['V021']['IDSPEC']:
            return Error(self.PRVS, message='001F.00.0661')

    @applicable_docs('R, D\d')     #H, T, C,  need join L
    def inspection__DET(self):
        if g.E('DET') and g.E('root.ZL_LIST.Z_SL.DATE_Z_1') and g.E('PACIENT.DR'):
            date1 = g.D('root.ZL_LIST.Z_SL.DATE_Z_1')
            date2 = g.D('PACIENT.DR')
            delta = relativedelta(date1, date2).years
            if delta >= 18:
                return Error(self.DET, message='002K.00.0520')

    @applicable_docs('H, T, C')
    def inspection__DATE_IN1(self):
        if g.E('DATE_IN') and g.E('root.ZL_LIST.SL.DISP'):
            date1 = g.D('DATE_IN')
            date2 = g.D('root.ZL_LIST.SL.DATE_1')
            if date1 < date2:
                return Error(self.DATE_IN, message='002K.00.0530')

    @applicable_docs('R, D\d')
    def inspection__DATE_IN2(self):
        if g.E('DATE_IN') and g.E('root.ZL_LIST.SL.DISP'):
            date1 = g.D('DATE_IN')
            date2 = g.D('root.ZL_LIST.SL.DATE_1')
            if date1 < date2:
                return Error(self.DATE_IN, message='002K.00.0540')

    @applicable_docs('X, D[A-Z]')
    def inspection__DATE_IN3(self):
        if g.E('DATE_IN') and g.E('SCHEET.DISP') and g.E('SL.DATE_1') and g.E('PERS.DR'):
            if g.S('SCHEET.DISP') in ['ДС1', 'ДС2', 'ДС3', 'ДС4', 'ПН1', 'ПН2']:
                delta = relativedelta(g.D('SL.DATE_1'), g.D('PERS.DR'))
                if delta.years >= 2:
                    return Error(self.DATE_IN, message='003K.00.0030')

    @applicable_docs('H, T, C, R, D\w, X')
    def inspection__DATE_IN4(self):
        if g.E('DATE_IN') and g.E('root.ZL_LIST.SL.DISP') and g.E('DATE_OUT'):
            date1 = g.D('DATE_IN')
            date2 = g.D('DATE_OUT')
            if date1 > date2:
                return Error(self.DATE_IN, message='002K.00.0550')

    @applicable_docs('X, D[A-Z]')
    def inspection__DATE_IN5(self):
        if g.E('DATE_IN') and g.E('root.ZL_LIST.SL.DISP') and g.E('root.ZL_LIST.SL.DATE_1') and g.E(
                'root.ZL_LIST.PERS.DR'):
            date1 = g.D('root.ZL_LIST.SL.DATE_1')
            date2 = g.D('root.ZL_LIST.PERS.DR')
            delta = relativedelta(date1, date2)
            if delta.years < 2:
                return Error(self.DATE_IN, message='002K.00.0530')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_IN6(self):
        if g.NE('DATE_IN') and g.E('IDSERV'):
            return Error(self, message='001K.00.0990')

    @applicable_docs('C, D, H, R, T, X, D\w')
    def inspection__DATE_OUT(self):
        if g.E('DATE_OUT') and g.E('root.ZL_LIST.SL.DISP') and g.E('root.ZL_LIST.SL.DATE_2'):
            date1 = g.D('DATE_OUT')
            date2 = g.D('root.ZL_LIST.SL.DATE_2')
            if date1 > date2:
                return Error(self.DATE_OUT, message='002K.00.0560')
        if g.NE('DATE_OUT') and g.E('IDSERV'):
            return Error(self, message='001K.00.1000')

    # @applicable_docs('H, T, DP, DV, DO, DS, DU, DF, C, R, D')
    # def inspection__DATE_OUT(self):
    #     if g.E('DATE_OUT') and g.E('root.ZL_LIST.SL.DISP') and g.E('DATE_IN'):
    #         date1 = g.D('DATE_OUT')
    #         date2 = g.D('DATE_IN')
    #         if date1 < date2:
    #             return Error(self.DATE_OUT, message='OUCH_DATE_OUT')

    @applicable_docs('H, T, D\w, C, R, X')
    def inspection__TARIF(self):
        if g.E('TARIF') and g.E('SUM_USL') and g.E('IDSERV'):
            if g.I('TARIF') > g.I('SUM_USL'):
                return Error(self.TARIF, message='002K.00.0570')

    @applicable_docs('C, R, D\w, H, T, X')
    def inspection__IDSERV(self):
        if g.NE('IDSERV') and g.E('LPU'):
            return Error(self, message='001K.00.0950')

    @applicable_docs('C, R, D\d, H, T')
    def inspection__DS(self):
        if g.NE('DS') and g.E('IDSERV'):
            return Error(self, message='001K.00.1010')

    @applicable_docs('C, R, D\w, H, T, X')
    def inspection__CODE_USL(self):
        if g.NE('CODE_USL') and g.E('IDSERV'):
            return Error(self, message='001K.00.1020')

    @applicable_docs('R, D\d')  # D отдельнио от профосмотров
    def inspection__USL(self):
        if g.NE('USL') and g.E('IDSERV'):
            return Error(self, message='001K.00.1030')

    @applicable_docs('R, D\w, C, H, T, X')
    def inspection__KOL_USL1(self):
        if g.NE('KOL_USL') and g.E('IDSERV'):
            return Error(self, message='001K.00.1040')

    @applicable_docs('R, D\d, C, H, T')
    def inspection__KOL_USL1(self):
        if g.E('KOL_USL') and g.E('USL_OK') and g.E('IDSP') and g.E('IDSERV'):
            if g.I('USL_OK') == 3 and g.I('IDSP') in [25, 28]:
                if g.I('KOL_USL') != 0:
                    return Error(self.KOL_USL, message='003K.00.1000')

    @applicable_docs('R, D\w, C, H, T, X')
    def inspection__SUMV_USL(self):
        if g.NE('SUMV_USL') and g.E('IDSERV'):
            return Error(self, message='001K.00.1050')

    @applicable_docs('R, D\w, C, H, T, X')
    def inspection__PRVS1(self):
        if g.NE('PRVS') and g.E('IDSERV'):
            return Error(self, message='001K.00.1060')

    @applicable_docs('C, H, T, D[A-Z], X')
    def inspection__CODE_MD(self):
        if g.NE('CODE_MD'):
            return Error(self, message='001K.00.1070')

    @applicable_docs('D[A-Z], X')
    def inspection__P_OTK(self):
        if g.NE('P_OTK'):
            return Error(self, message='001K.00.1080')

    @applicable_docs('D/d, R')
    def inspection__VID_VME(self):
        if g.E('VID_VME') and g.E('VIDPOM'):
            if g.I('VIDPOM') == 32:
                check = re.fullmatch('[a-zA-zа-яА-Я][a-zA-zа-яА-Я][0-9]', g.S('VID_VME'))
                if not check:
                    return Error(self.VID_VME, message='004F.00.1400')
            else:
                check = re.fullmatch(
                    '[a-zA-Zа-яА-Я][0-9]{2}\.[0-9]{2}[a-zA-Zа-яА-Я0-9]?\.[0-9]{3}(\.[a-zA-zа-яА-Я0-9]{3})?$',
                    g.S('VID_VME'))
                if not check:
                    return Error(self.VID_VME, message='004F.00.1390')

    @applicable_docs('T')
    def inspection__VID_VME2(self):
        if g.E('VID_VME'):
            check = re.fullmatch('^[a-zA-Zа-яА-Я0-9]{2}[0-9]$', g.S('VID_VME'))
            if not check:
                return Error(self.VID_VME, message='004F.00.1380')

    @applicable_docs('C, H')
    def inspection__VID_VME3(self):
        if g.E('VID_VME'):
            check = re.fullmatch(
                '[a-zA-Zа-яА-Я][0-9]{2}\.[0-9]{2}[a-zA-Zа-яА-Я0-9]?\.[0-9]{3}(\.[a-zA-zа-яА-Я0-9]{3})?$',
                g.S('VID_VME'))
            if not check:
                return Error(self.VID_VME, message='004F.00.1370')

    @applicable_docs('T')
    def inspection__VID_VME5(self):
        if g.E('USL_TIP'):
            if ('C00' <= g.S('DS1') < 'D10') or (
                    g.S('DS1') == 'D70' and ('C00' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')) and g.I(
                'USL_TIP') in [1, 3, 4]:
                if g.NE('VID_VME'):
                    return Error(self, message='003F.00.1510')
            if ('C00' <= g.S('DS1') < 'D10') and g.I('USL_TIP') in [1, 3, 4]:
                if g.NE('VID_VME'):
                    return Error(self, message='003F.00.1511')

    @applicable_docs('C, D/d, R')
    def inspection__VID_VME4(self):
        if g.E('USL_TIP'):
            if ('C00' <= g.S('DS1') < 'D10') or (
                    g.S('DS1') == 'D70' and ('C00' <= g.S('DS2') < 'C81' or g.S('DS2') == 'C97')) and g.I(
                'USL_TIP') in [1, 3, 4, 6]:
                if g.NE('VID_VME'):
                    return Error(self, message='003F.00.1520')
            if ('C00' <= g.S('DS1') < 'D10') and g.I('USL_TIP') in [1, 3, 4, 6]:
                if g.NE('VID_VME'):
                    return Error(self, message='003F.00.1521')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__IDSERV11(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('LPU'):
            if g.NE('IDSERV'):
                return Error(self, message='003F.00.2850')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__LPU11(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('LPU'):
                return Error(self, message='003F.00.2860')

    @applicable_docs('C, R, T, D\d, H')
    def inspection__PRFIL12(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('PROFIL'):
                return Error(self, message='003F.00.2870')

    @applicable_docs('C, R, T, D\d, H')
    def inspection__DET21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('DET'):
                return Error(self, message='003F.00.2880')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__DATE_IN21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('DATE_IN'):
                return Error(self, message='003F.00.2890')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__DATE_OUT21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('DATE_OUT'):
                return Error(self, message='003F.00.2900')

    @applicable_docs('C, R, T, D\d, H')
    def inspection__DS21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('DS'):
                return Error(self, message='003F.00.2910')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__CODE_USL21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('CODE_USL'):
                return Error(self, message='003F.00.2920')

    @applicable_docs('R,D\d')
    def inspection__USL21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('USL'):
                return Error(self, message='003F.00.2930')

    @applicable_docs('C, R, T, D\d, H')
    def inspection__KOL_USL21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('KOL_USL'):
                return Error(self, message='003F.00.2940')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__SUMV_USL21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('SUMV_USL'):
                return Error(self, message='003F.00.2950')

    @applicable_docs('C, R, T, D\w, H, X')
    def inspection__PRVS21(self):  # end 31/03/20
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL'):
            if g.NE('PRVS'):
                return Error(self, message='003sF.00.2960')

    @applicable_docs('C, R, T, D\d, H')
    def inspection__PRVS21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('PRVS'):
                return Error(self, message='003F.00.2961')

    @applicable_docs('X, D[A-Z]')
    def inspection__PRVS22(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV') and g.E('P_OTK'):
            if g.I('P_OTK') == 0:
                if g.NE('PRVS'):
                    return Error(self, message='003F.00.2962')
            if g.I('P_OTK') == 1:
                if g.E('PRVS'):
                    return Error(self, message='003F.00.2963')

    @applicable_docs('C, T, H')
    def inspection__CODE_MD21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('CODE_MD'):
                return Error(self, message='003F.00.2971')

    @applicable_docs('X, D[A-Z]')
    def inspection__CODE_MD22(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV') and g.E('P_OTK'):
            if g.I('P_OTK') == 0:
                if g.NE('CODE_MD'):
                    return Error(self, message='003F.00.2972')
            if g.I('P_OTK') == 1:
                if g.E('CODE_MD'):
                    return Error(self, message='003F.00.2973')

    @applicable_docs('X, D[A-Z]')
    def inspection__P_OTK21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.USL') and g.E('IDSERV'):
            if g.NE('P_OTK'):
                return Error(self, message='003F.00.2980')


class ONK_USL(AbsModel):
    @applicable_docs('T, C, R, D\d')
    def inspection__USL_TIP(self):
        if g.NE('USL_TIP'):
            return Error(self, message='001K.00.1110')

    @applicable_docs('C, R, D\d')
    def inspection__HIR_TIP(self):
        if g.E('USL_TIP') and g.I('USL_TIP') == 1:
            if g.NE('HIR_TIP'):
                return Error(self, message='003F.00.1410')
        elif g.I('USL_TIP') != 1:
            if g.E('HIR_TIP'):
                return Error(self.HIR_TIP, message='003F.00.1420')
        if g.E('HIR_TIP'):
            if g.S('HIR_TIP') not in app.td['N014']['ID_THir']:
                return Error(self.HIR_TIP, message='001F.00.0520')

    @applicable_docs('C, R, D\d, T')
    def inspection__LEK_TIP_L(self):
        if g.E('USL_TIP') and g.I('USL_TIP') == 2:
            if g.NE('LEK_TIP_L'):
                return Error(self, message='003F.00.1430')
        elif g.I('USL_TIP') != 2:
            if g.E('LEK_TIP_L'):
                return Error(self.LEK_TIP_L, message='003F.00.1440')
        if g.E('LEK_TIP_L'):
            if g.S('LEK_TIP_L') not in app.td['N015']['ID_TLek_L']:
                return Error(self.LEK_TIP_L, message='001F.00.0530')

    @applicable_docs('C, R, D\d, T')
    def inspection__LEK_TIP_V(self):
        if g.E('USL_TIP') and g.I('USL_TIP') != 2:
            if g.E('LEK_TIP_V'):
                return Error(self.LEK_TIP_V, message='003F.00.1460')
        elif g.I('USL_TIP') == 2:
            if g.NE('LEK_TIP_V'):
                return Error(self, message='003F.00.1450')
        if g.E('LEK_TIP_V'):
            if g.S('LEK_TIP_V') not in app.td['N016']['ID_TLek_V']:
                return Error(self.LEK_TIP_V, message='001F.00.0540')

    @applicable_docs('C, R, D\d, T')
    def inspection__LUCH_TIP_1(self):
        if g.E('USL_TIP') and g.I('USL_TIP') in [3, 4]:
            if g.NE('LUCH_TIP'):
                return Error(self, message='003F.00.1470')
        elif g.I('USL_TIP') not in [3, 4]:
            if g.E('LUCH_TIP'):
                return Error(self.LUCH_TIP, message='003F.00.1480')
        if g.E('LUCH_TIP'):
            if g.S('LUCH_TIP') not in app.td['N017']['ID_TLuch']:
                return Error(self.LUCH_TIP, message='001F.00.0550')

    @applicable_docs('C, R, D\d, T')
    def inspection__LEK_PR(self):
        if g.E('USL_TIP'):
            if g.I('USL_TIP') in [2, 4]:
                if g.NE('LEK_PR'):
                    return Error(self, message='003F.00.1490')
            if g.I('USL_TIP') not in [2, 4]:
                if g.E('LEK_PR'):
                    return Error(self.LEK_PR, message='003F.00.1500')

    @applicable_docs('C, T, R, D\d')
    def inspection__USL_TIP21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.ONK_SL.ONK_USL'):
            if g.NE('USL_TIP'):
                return Error(self, message='003F.00.3010')


class VERS_SPEC(AbsModel):
    pass


class PERS(AbsModel):

    @applicable_docs('L, LT, LP, LV, LO, LS, LU, LF, LC')
    def inspection__W(self):
        if g.E('W') and g.S('W') not in app.td['V005']['IDPOL']:
            return Error(self.W, message='ohW')

    @applicable_docs('L')
    def inspection__DOCTYPE_1(self):
        if g.E('DOCTYPE') and g.S('DOCTYPE') not in app.td['F011']['IDDoc']:
            return Error(self.DOCTYPE, message='001F.00.0720')

    @applicable_docs('L')
    def inspection__DOCTYPE_4(self):
        if g.E('DOCTYPE') and g.E('root.ZL_LIST.ZAP.PACIENT.VPOLIS'):
            if g.I('root.ZL_LIST.ZAP.PACIENT.VPOLIS') != 3:
                return Error(self.DOCTYPE, message='003F.00.1720')

    @applicable_docs('LC, L, LT, LP, LV, LO, LS, LU, LF')
    def inspection__DR1(self):
        if g.E('DR') and g.E('root.PERS_LIST.ZGLV.DATA'):
            date1 = g.D('DR')
            date2 = g.D('root.PERS_LIST.ZGLV.DATA')
            if date1 > date2:
                return Error(self.DR, message='002K.00.0670')

    @applicable_docs('L')
    def inspection__DR2(self):
        if g.E('DR') and g.E('root.ZL_LIST.Z_SL.DATE_Z_1'):
            date1 = g.D('root.ZL_LIST.Z_SL.DATE_Z_1')
            date2 = g.D('DR')
            if date2 >= date1:
                return Error(self.DR, message='002K.00.0680')

    @applicable_docs('L')
    def inspection__DR4(self):
        if g.E('DR') and g.E('DOCTYPE') and g.E('root.PERS_LIST.ZGLV.DATA'):
            if g.I('DOCTYPE') in [1, 4, 5, 6, 7, 8, 14, 16, 17, 26, 29]:
                date1 = g.D('root.PERS_LIST.ZGLV.DATA')
                date2 = g.D('DR')
                delta = relativedelta(date1, date2).years
                if delta < 14:
                    if g.E('DR_P'):
                        date3 = g.D('DR_P')
                        delta2 = relativedelta(date1, date3).years
                        if delta2 < 14:
                            return Error(self.DR, message='003K.00.1020')
                    else:
                        return Error(self.DR, message='003K.00.1020')

    @applicable_docs('D\d, R')
    def inspection__DR5(self):
        if g.E('DR') and g.E('DOCTYPE'):
            if g.I('DOCTYPE') in [1, 4, 5, 6, 7, 8, 14, 16, 17, 26, 29]:
                date1 = datetime.now()
                date2 = g.D('DR')
                delta = relativedelta(date1, date2).years
                if delta < 14:
                    return Error(self.DR, message='003K.00.1030')

    @applicable_docs('X, D[A-Z]')
    def inspection__DR3(self):
        if g.E('DR') and g.E('root.ZL_LIST.Z_SL.DATE_1') and g.E('SCHET.DISP'):
            date1 = g.D('root.ZL_LIST.Z_SL.DATE_1')
            date2 = g.D('DR')
            delta = relativedelta(date1, date2).years
            if g.S('SCHET.DISP') in ['ДВ4', 'ДВ2', 'ОПВ']:
                if delta <= 18:
                    return Error(self.DR, message='003K.00.0050')
            elif g.S('SCHET.DISP') in ['ДС1', 'ДС2', 'ДС3', 'ДС4', 'ПН1', 'ПН2']:
                if delta > 18:
                    return Error(self.DR, message='003K.00.0060')

    @applicable_docs('LC, LT, LP, LV, LO, LS, LU, LF')
    def inspection__DR_P1(self):
        if g.E('DR_P') and g.E('root.ZL_LIST.Z_SL.DATE_Z_1'):
            date1 = g.D('root.ZL_LIST.Z_SL.DATE_Z_1')
            date2 = g.D('DR_P')
            delta = relativedelta(date1, date2).years
            if delta <= 14:
                # if int(g.S('root.ZL_LIST.Z_SL.DATE_Z_1')[0:4]) \
                #        - int(g.S('DR_P')[0:4]) <= 14:
                return Error(self.DR_P, message='OUCHDR_P1')

    @applicable_docs('L')
    def inspection__DR_P2(self):
        if g.E('DR_P') and g.E('DR'):
            date1 = g.D('DR')
            date2 = g.D('DR_P')
            delta = relativedelta(date2, date1).years
            if delta <= 14:
                return Error(self.DR_P, message='002K.00.0690')

    @applicable_docs('L')
    def inspection__DOCSER(self):
        if g.E('DOCSER') and g.E('DOCTYPE'):
            if not g.S('DOCSER') is None:
                if g.I('DOCTYPE') in g.MaskList:
                    check = re.fullmatch(g.RegMask[g.I('DOCTYPE') - 1][1], g.S('DOCSER'))
                    if not check:
                        check = re.fullmatch(g.RegMask[g.I('DOCTYPE') - 1][2], g.S('DOCSER'))
                else:
                    check = re.fullmatch(g.RegMask[g.I('DOCTYPE') - 1][1], g.S('DOCSER'))
                    if not check:
                        return Error(self.DOCSER, message='004F.00.1700')
            else:
                return Error(self.DOCSER, message='004F.00.1700')
            if g.E('root.ZL_LIST.ZAP.PACIENT.VPOLIS'):
                if g.I('root.ZL_LIST.ZAP.PACIENT.VPOLIS') != 3:
                    return Error(self.DOCSER, message='003F.00.1730')

    @applicable_docs('L')
    def inspection__DOCNUM(self):
        if g.E('DOCNUM') and g.E('DOCTYPE'):
            if not g.S('DOCNUM') is None:
                if g.I('DOCTYPE') in g.MaskList:
                    check = re.fullmatch(g.RegMask[g.I('DOCTYPE') - 1][3], g.S('DOCNUM'))
                else:
                    check = re.fullmatch(g.RegMask[g.I('DOCTYPE') - 1][2], g.S('DOCNUM'))
                if not check:
                    return Error(self.DOCNUM, message='004F.00.1710')
            else:
                return Error(self.DOCNUM, message='004F.00.1710')
        if g.E('root.ZL_LIST.ZAP.PACIENT.VPOLIS'):
            if g.I('root.ZL_LIST.ZAP.PACIENT.VPOLIS') != 3:
                return Error(self.DOCSER, message='003F.00.1740')

    @applicable_docs('L')
    def inspection__FAM(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') == 0 and g.I('DOST') != 2:
                if g.NE('FAM'):
                    return Error(self, message='003F.00.1530')
            if g.I('NOVOR') != 0 or g.I('DOST') == 2:
                if g.E('FAM'):
                    return Error(self.FAM, message='003F.00.1540')

    @applicable_docs('L')
    def inspection__FAM_P(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') != 0 and g.I('DOST') != 2:
                if g.E('FAM_P'):
                    return Error(self.FAM_P, message='003F.00.1550')
            if g.I('NOVOR') == 0 or g.I('DOST') == 2:
                if g.E('FAM_P'):
                    return Error(self.FAM_P, message='003F.00.1560')

    @applicable_docs('L')
    def inspection__IM(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') == 0 and g.I('DOST') != 3:
                if g.NE('IM'):
                    return Error(self, message='003F.00.1570')
            if g.I('NOVOR') != 0 or g.I('DOST') == 3:
                if g.E('IM'):
                    return Error(self.IM, message='003F.00.1580')

    @applicable_docs('L')
    def inspection__IM_P(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') != 0 and g.I('DOST') != 3:
                if g.E('IM_P'):
                    return Error(self.IM_P, message='003F.00.1590')
            if g.I('NOVOR') == 0 or g.I('DOST') == 3:
                if g.E('IM_P'):
                    return Error(self.IM_P, message='003F.00.1600')

    @applicable_docs('L')
    def inspection__OT(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') != 0 or g.I('DOST') == 1:
                if g.E('OT'):
                    return Error(self.OT, message='003F.00.1610')

    @applicable_docs('L')
    def inspection__OT_P(self):
        if g.E('NOVOR') and g.E('DOST'):
            if g.I('NOVOR') == 0 or g.I('DOST') == 1:
                if g.E('OT_P'):
                    return Error(self.OT_P, message='003F.00.1620')

    @applicable_docs('L')
    def inspection__DOST2(self):
        if g.E('NOVOR'):
            if g.NE('IM') and g.I('NOVOR') == 0:
                if g.NE('DOST'):
                    return Error(self, message='003F.00.1630')
            if g.NE('FAM') and g.I('NOVOR') == 0:
                if g.NE('DOST'):
                    return Error(self, message='003F.00.1640')
            if g.NE('IM_P') and g.I('NOVOR') != 0:
                if g.NE('DOST'):
                    return Error(self, message='003F.00.1650')
            if g.NE('FAM_P') and g.I('NOVOR') != 0:
                if g.NE('DOST'):
                    return Error(self, message='003F.00.1660')

    @applicable_docs('L')
    def inspection__W_P(self):
        if g.E('NOVOR'):
            if g.I('NOVOR') != 0:
                if g.NE('W_P'):
                    return Error(self, message='003F.00.1670')
            if g.I('NOVOR') == 0:
                if g.E('W_P'):
                    return Error(self.W_P, message='003F.00.1680')

    @applicable_docs('L')
    def inspection__DR_P(self):
        if g.E('NOVOR'):
            if g.I('NOVOR') != 0:
                if g.NE('DR_P'):
                    return Error(self, message='003F.00.1690')
            if g.I('NOVOR') == 0:
                if g.E('DR_P'):
                    return Error(self.DR_P, message='003F.00.1700')

    @applicable_docs('L')
    def inspection__ID_PAC12(self):
        if g.NE('ID_PAC'):
            return Error(self, message='003F.00.3080')

    @applicable_docs('L')
    def inspection__W12(self):
        if g.NE('W'):
            return Error(self, message='003F.00.3090')

    @applicable_docs('L')
    def inspection__DR12(self):
        if g.NE('DR'):
            return Error(self, message='003F.00.3100')

    # @applicable_docs('L')
    # def inspection__TEL(self):


class NAPR(AbsModel):

    @applicable_docs('T, C, R, D\d')
    def inspection__NAPR_V(self):
        if g.E('NAPR_V') and g.S('NAPR_V') not in app.td['V028']['IDVN']:
            return Error(self.NAPR_V, message='ohNAPR_V')

    @applicable_docs('T, C, R, D\d')
    def inspection__NAPR_ISSL(self):
        if g.E('NAPR_ISSL') and g.E('NAPR_V'):
            if g.I('NAPR_V') == 3:
                if g.S('NAPR_ISSL') not in app.td['V029']['IDMET']:
                    return Error(self.NAPR_ISSL, message='ohNAPR_ISSL')

    @applicable_docs('R, C, T, D\d')
    def inspection__NAPR_DATE(self):
        if g.E('NAPR_DATE') and g.E('root.ZL_LIST.SL.DISP'):
            date1 = g.D('root.ZL_LIST.ZAP.Z_SL.SL.DATE_2')
            date2 = g.D('NAPR_DATE')
            if date2 >= date1:
                return Error(self.NAPR_DATE, message='002K.00.0580')
            if g.NE('NAPR'):
                return Error(self.NAPR_DATE, message='001K.00.1090')

    @applicable_docs('R, C, T, D\d')
    def inspection__NAPR_DATE(self):
        if g.E('NAPR_DATE') and g.E('root.ZL_LIST.SL.DISP'):
            date1 = g.D('root.ZL_LIST.ZAP.Z_SL.SL.DATE_1')
            date2 = g.D('NAPR_DATE')
            if date2 <= date1:
                return Error(self.NAPR_DATE, message='002K.00.0590')

    @applicable_docs('T, C, R, D\d')
    def inspection__MET_ISSL(self):
        if g.E('NAPR_V'):
            if g.I('NAPR_V') == 3:
                if g.NE('MET_ISSL'):
                    return Error(self, message='003F.00.1190')
            if g.I('NAPR_V') != 3:
                if g.E('MET_ISSL'):
                    return Error(self.MET_ISSL, message='003F.00.1200')

    @applicable_docs('T, C, R, D\d')
    def inspection__NAPR_USL(self):
        if g.E('MET_ISSL'):
            if g.NE('NAPR_USL'):
                return Error(self, message='003F.00.1210')
        if g.NE('MET_ISSL'):
            if g.E('NAPR_USL'):
                return Error(self.NAPR_USL, message='003F.00.1220')

    @applicable_docs('C, T, R, D\d')
    def inspection__NAPR_DATE21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.NAPR'):
            if g.NE('NAPR_DATE'):
                return Error(self, message='003F.00.2990')

    @applicable_docs('C, T, R, D\d')
    def inspection__NAPR_V21(self):
        if g.E('root.ZL_LIST.ZAP.Z_SL.SL.NAPR'):
            if g.NE('NAPR_V'):
                return Error(self, message='003F.00.3000')


class CONS(AbsModel):

    @applicable_docs('T, R, D\d')
    def inspection__PR_CONS1(self):
        if g.NE('PR_CONS'):
            return Error(self, message='001K.00.0680')

    @applicable_docs('C')
    def inspection__PR_CONS2(self):
        if g.E('PR_CONS'):
            if g.S('PR_CONS') not in app.td['N019']['ID_CONS']:
                return Error(self.PR_CONS, message='ohPR_CONS2')

    @applicable_docs('T, C, R, D\d')
    def inspection__DT_CONS(self):
        if g.E('DT_CONS') and g.E('root.ZL_LIST.SL.DATE_2'):
            date1 = g.D('root.ZL_LIST.SL.DATE_2')
            date2 = g.D('DT_CONS')
            # if g.D('DT_CONS')
            if date2 >= date1:
                return Error(self.DT_CONS, message='002K.00.0600')

        if g.E('PR_CONS'):
            if g.I('PR_CONS') in [1, 2, 3]:
                if g.NE('DT_CONS'):
                    return Error(self, message='003F.00.1230')
            if g.I('PR_CONS') not in [1, 2, 3]:
                if g.E('DT_CONS'):
                    return Error(self.DT_CONS, message='003F.00.1240')

    @applicable_docs('C, D\d, R, T')
    def inspection__PR_CONS11(self):
        if g.E('CONS'):
            if g.NE('PR_CONS'):
                return Error(self.PR_CONS, message='003F.00.2580')


class LEK_PR(AbsModel):

    @applicable_docs('C, R,  T, D\d')
    def inspection__DATE_INJ1(self):
        if g.E('DATE_INJ') and g.E('root.ZL_LIST.SL.DATE_1'):
            date1 = g.D('root.ZL_LIST.SL.DATE_1')
            date2 = g.D('DATE_INJ')
            if date2 <= date1:
                return Error(self.DATE_INJ, message='002K.00.0610')

    @applicable_docs('C, R, T, D\d')
    def inspection__DATE_INJ2(self):
        if g.E('DATE_INJ') and g.E('root.ZL_LIST.SL.DATE_2'):
            date1 = g.D('root.ZL_LIST.SL.DATE_2')
            date2 = g.D('DATE_INJ')
            if date2 >= date1:
                return Error(self.DATE_INJ, message='002K.00.0620')
        if g.NE('DATE_INJ'):
            return Error(self, message='001K.00.0780')

    @applicable_docs('C, R, T, D\d')
    def inspection__CODE_SH1(self):
        if g.E('CODE_SH') and g.E('DS1'):
            if ('C81' > g.S('DS1') >= 'C97'):
                return Error(self.CODE_SH, message='002K.00.0630')

    @applicable_docs('C, R, T, D\d')
    def inspection__CODE_SH2(self):
        if g.E('CODE_SH') and g.E('root.ZL_LIST.Z_SL.DATE_Z_1') and g.E('PACIENT.DR'):
            if int(g.S('root.ZL_LIST.Z_SL.DATE_Z_1')[0:4]) \
                    - int(g.S('PACIENT.DR')[0:4]) > 18:
                return Error(self.CODE_SH, message='002K.00.0640')

    @applicable_docs('C, R, T, D\d')
    def inspection__CODE_SH3(self):
        if g.E('CODE_SH') and g.E('root.ZL_LIST.Z_SL.DATE_Z_1') and g.E('PACIENT.DR') and g.E('DS') and g.E('USL_TIP'):
            if int(g.S('root.ZL_LIST.Z_SL.DATE_Z_1')[0:4]) \
                    - int(g.S('PACIENT.DR')[0:4]) < 18 and (
                    ('C97' > g.S('DS1') >= 'D10') or ('C00' > g.S('DS1') >= 'C81')) and g.I('USL_TIP') != 2:
                return Error(self.CODE_SH, message='002K.00.0650')

    @applicable_docs('C, R, T, D\d')
    def inspection__CODE_SH4(self):
        if g.E('CODE_SH') and g.E('root.ZL_LIST.Z_SL.DATE_Z_1') and g.E('PACIENT.DR') and g.E('DS') and g.E('USL_TIP'):
            if int(g.S('root.ZL_LIST.Z_SL.DATE_Z_1')[0:4]) \
                    - int(g.S('PACIENT.DR')[0:4]) < 18 and (
                    ('C97' > g.S('DS1') >= 'D10') or ('C00' > g.S('DS1') >= 'C81')) and g.I('USL_TIP') != 4:
                return Error(self.CODE_SH, message='002K.00.0660')
            if g.NE('LEK_PR'):
                return Error(self.CODE_SH, message='001K.00.0770')

    @applicable_docs('C, R, T, D\d')
    def inspection__REGNUM(self):
        if g.NE('REGNUM'):
            return Error(self, message='001K.00.0760')
        if g.E('LEK_PR'):
            if g.NE('REGNUM'):
                return Error(self, message='003F.00.2660')

    @applicable_docs('C, R, T, D\d')
    def inspection__CODE_SH11(self):
        if g.E('LEK_PR'):
            if g.NE('CODE_SH'):
                return Error(self, message='003F.00.2670')

    @applicable_docs('C, R, T, D\d')
    def inspection__DATE_INJ11(self):
        if g.E('LEK_PR'):
            if g.NE('DATE_INJ'):
                return Error(self, message='003F.00.2680')
