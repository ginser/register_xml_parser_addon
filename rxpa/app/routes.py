import os
import re
from json import dumps

from flask import g, abort, request, after_this_request
from werkzeug.utils import secure_filename

import app as app_package
from app import app
from app.models import ShortConditionalFuncs
from app.tags_dict_init import get_tags_dict
from app.xml_checker import XMLchecker, JoinXMLchecker
from app.auxiliary_file import parse_aux_file


class FilesNotFoundException(Exception):
    pass


class JoinRuleNotFoundException(Exception):
    pass


def is_files_exist(files_list):
    exist = True
    for file in files_list:
        if file not in request.files or not request.files[file]:
            exist = False
    return exist


def get_filenames(files_list):
    filenames = []
    for file in files_list:
        file_obj = request.files[file]
        filenames.append(file_obj.filename)
    return filenames


def get_filepaths(files_list):
    filepaths = []
    for file in files_list:
        file_obj = request.files[file]
        filename = secure_filename(file_obj.filename)
        filepaths.append(os.path.join(app.config['UPLOAD_DIR'], filename))
    return filepaths


def save_files(files_list, filepaths):
    for file, filepath in zip(files_list, filepaths):
        file_obj = request.files[file]
        file_obj.save(filepath)


def remove_files(filepaths):
    for filepath in filepaths:
        try:
            os.remove(filepath)
        except Exception as e:
            app.logger.exception(f'Error removing file {filepath}')


def get_prep_work(files_list):
    g.scf = ShortConditionalFuncs()
    for func in 'I S E NE MKB D F'.split():
        setattr(g, func, getattr(g.scf, func))

    if not is_files_exist(files_list):
        raise FilesNotFoundException('Not found required file(s) in request')
    filepaths = get_filepaths(files_list)
    filenames = get_filenames(files_list)
    save_files(files_list, filepaths)

    return filenames, filepaths


def get_multiple_file_join_rule(filenames):
    def is_equal_rulenames_filenames(rulenames, filenames):
        equal = True
        for rulename, filename in zip(rulenames, filenames):
            if re.match(rulename, filename) is None:
                equal = False
        return equal

    for rulename_str, rule in app.config['MULTIPLE_FILE_CHECK_JOIN_RULES'].items():
        rulenames = rulename_str.split(', ')
        if is_equal_rulenames_filenames(rulenames, filenames):
            return rule
    raise JoinRuleNotFoundException('No matching join rule found')


@app.route('/', methods=['POST'])
def check_one_file():
    files_list = ['file']
    filepaths = []
    try:
        filenames, filepaths = get_prep_work(files_list)
    except FilesNotFoundException:
        app.logger.exception('Error while preparing for check file')
        return str(e), 400

    log, errors = XMLchecker(filepaths[0]).check_xml()

    @after_this_request
    def after_remove_files(response):
        remove_files(filepaths)
        return response

    return dumps(
        dict(log=log, errors=errors), indent=4, ensure_ascii=False
    )


@app.route('/check_two_files', methods=['POST'])
def check_two_files():
    files_list = ['main_file', 'aux_file']
    filepaths = []
    try:
        filenames, filepaths = get_prep_work(files_list)
        rule = get_multiple_file_join_rule(filenames)
    except (FilesNotFoundException, JoinRuleNotFoundException) as e:
        app.logger.exception('Error while preparing for check join files')
        return str(e), 400

    aux_dict = parse_aux_file(filepaths[1], rule)
    xmlchecker = JoinXMLchecker(filepaths[0], aux_dict, rule)
    log, errors = xmlchecker.check_xml()

    @after_this_request
    def after_remove_files(response):
        remove_files(filepaths)
        return response

    return dumps(
        dict(log=log, errors=errors), indent=4, ensure_ascii=False
    )


@app.route('/reload_tags_dict')
def reload_tags_dict():
    try:
        app_package.td = get_tags_dict(force=True)
    except Exception as e:
        app.logger.exception('get_tags_dict crashed')
        abort(500)
    return 'ok'
