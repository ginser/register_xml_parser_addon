import inspect
import logging
import os
import xml.sax
from io import StringIO

from app import inspections
from app.models import AbsModel

logger = logging.getLogger('xml_parser')


class RegisterXmlHandler(xml.sax.ContentHandler):
    def __init__(self, report_file, filename):
        super().__init__()
        self.root = inspections.XML_ROOT(report_file, filename=filename)
        self.cursor = self.root
        self.skip_tag = None
        self.class_list = dict(inspect.getmembers(inspections, inspect.isclass))
        self.errors = []

    def startElement(self, tag, attributes):
        if tag in self.class_list:
            child_class = self.class_list[tag]
        else:
            child_class = AbsModel.create_empty_child(tag)
        child = child_class(parent=self.cursor, root=self.root)
        setattr(self.cursor, tag, child)
        self.cursor = getattr(self.cursor, tag)
        logger.debug(f'Set {tag}')

    def characters(self, content):
        if not any(x in content for x in '\n\t'):
            existed_content = self.cursor.content or ''
            self.cursor.content = existed_content + content

    def endElement(self, tag):
        self.cursor.line = self._locator.getLineNumber()
        self.cursor.column = self._locator.getColumnNumber()
        self.errors.extend(self.cursor.make_inspections())
        self.cursor = self.cursor.parent
        logger.debug(f'Returned from {tag}')


class AuxFileRegisterXmlHandler(RegisterXmlHandler):
    def endElement(self, tag):
        self.cursor = self.cursor.parent
        logger.debug(f'Returned from {tag}')


class JoinFilesRegisterXmlHandler(RegisterXmlHandler):
    def __init__(self, report_file, filename, aux_dict, rule):
        super().__init__(report_file, filename)
        self.aux_dict = aux_dict
        self.rule = rule

    def endElement(self, tag):
        self.cursor.prepare_aux_part(self.aux_dict, self.rule)
        super().endElement(tag)


class AbstractXMLchecker:
    def __init__(self):
        self.filename = None
        self.report_file = None
        self.handler = None
        self.parser = None

    def _prepare(self, handler_class, *args, **kwargs):
        parser = xml.sax.make_parser()
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)
        handler = handler_class(self.report_file, self.filename, *args, **kwargs)
        parser.setContentHandler(handler)

        self.handler = handler
        self.parser = parser

    def _check_file_xml(self):
        with open(self.filepath, 'r', encoding='cp1251') as f:
            self.parser.parse(f)
        self.report_file.seek(0)
        report = self.report_file.getvalue()
        return report, [repr(err) for err in self.handler.errors]

    def _check_xml(self, *args, **kwargs):
        pass

    def check_xml(self, *args, **kwargs):
        self.report_file = StringIO()
        logger.info(f'Starting check for {self.filename}')
        report, errors = self._check_xml(*args, **kwargs)
        logger.info(f'Checking ended for {self.filename}')
        self.report_file.close()
        return report, errors


class XMLchecker(AbstractXMLchecker):
    def __init__(self, filepath):
        super().__init__()
        self.filename = os.path.basename(filepath)
        self.filepath = filepath

    def _check_xml(self):
        self._prepare(RegisterXmlHandler)
        return self._check_file_xml()


class AuxXMLchecker(AbstractXMLchecker):
    def __init__(self, filename):
        super().__init__()
        self.filename = filename

    def _check_xml(self, xml_file, *args, **kwargs):
        self._prepare(AuxFileRegisterXmlHandler)
        self.parser.parse(xml_file)

        return None, None


class JoinXMLchecker(AbstractXMLchecker):
    def __init__(self, filepath, aux_dict, rule):
        super().__init__()
        self.filename = os.path.basename(filepath)
        self.filepath = filepath
        self.aux_dict = aux_dict
        self.rule = rule

    def _check_xml(self, *args, **kwargs):
        self._prepare(
            JoinFilesRegisterXmlHandler, aux_dict=self.aux_dict, rule=self.rule
        )

        return self._check_file_xml()