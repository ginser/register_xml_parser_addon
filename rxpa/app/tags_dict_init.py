import os
import xml.etree.ElementTree as ET
import local_config as lc
from datetime import datetime
from itertools import chain
from json import dumps, loads

from app import app


def find_tags(root, tag):
    tag_values = []
    for child in root.findall(f'.//{tag}'):
        tag_values.append(child.text)
    return tag_values


def filter_tags_dict(tags_dict):
    filtered_dict = []
    try:
        curr_date = datetime.strptime(lc.Check.date_ctrl, '%d.%m.%Y')
    except Exception:
        curr_date = datetime.now()
    for value in tags_dict:
        if value[-1] is None or datetime.strptime(
                value[-1], '%d.%m.%Y') >= curr_date:
            filtered_dict.append(value[:-1])
    return filtered_dict


def linearize_tag_values(tags_dict):
    if len(tags_dict) and (len(tags_dict[0]) == 1):
        return list(chain.from_iterable(tags_dict))
    return tags_dict


def _tags_dict_init(tags_dict_path, extracted_tags_dict_path, req_tags):
    tags_dict = dict()
    for filename, tags in req_tags.items():
        app.logger.info(f'Processing {filename} dict')
        tree = ET.parse(os.path.join(tags_dict_path, filename + '.xml'))
        root = tree.getroot()
        tags_dict[filename] = dict()
        tag_values = []

        for field_str in tags['fields']:
            fields = field_str.split(', ')
            if not tags.get('disable_dateend', False):
                fields.append('DATEEND')

            for field in fields:
                tag_values.append(find_tags(root, field))

            if len(set(map(len, tag_values))) != 1:
                raise Exception(
                    f'Разное количество сопоставляемых тегов для {filename}')

            tag_values = list(set(zip(*tag_values)))
            if not tags.get('disable_dateend', False):
                tag_values = filter_tags_dict(tag_values)
            tag_values = linearize_tag_values(tag_values)
            tags_dict[filename][field_str] = tag_values

    with open(extracted_tags_dict_path, 'w') as f:
        f.write(dumps(tags_dict, indent=4, ensure_ascii=False))


def get_tags_dict(force=False):
    tags_dict_path = app.config['TAGS_DICT_DIR']
    extracted_tags_dict_path = os.path.join(app.instance_path, 'tags_dict.json')

    if force:
        _tags_dict_init(tags_dict_path, extracted_tags_dict_path,
                        app.config['TAGS_DICT'])
        with open(extracted_tags_dict_path, 'r') as f:
            tags_dict = loads(f.read())
    else:
        try:
            with open(extracted_tags_dict_path, 'r') as f:
                tags_dict = loads(f.read())
        except Exception as e:
            app.logger.info(
                'Failed to load tags_dict.json\nCreating new one',
                exc_info=True
            )

            _tags_dict_init(tags_dict_path, extracted_tags_dict_path,
                            app.config['TAGS_DICT'])
            with open(extracted_tags_dict_path, 'r') as f:
                tags_dict = loads(f.read())

    return tags_dict
