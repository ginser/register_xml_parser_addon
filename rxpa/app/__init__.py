import logging.config
import os

from flask import Flask

from config import Config

if not os.path.exists(Config.LOG_DIR):
    os.makedirs(Config.LOG_DIR)

logging.config.dictConfig(Config.LOGGING_CONFIG)

app = Flask(__name__)
app.config.from_object(Config)

if not os.path.exists(app.instance_path):
    os.makedirs(app.instance_path)

app.config['UPLOAD_DIR'] = os.path.join(
    app.instance_path, 'uploads')
if not os.path.exists(app.config['UPLOAD_DIR']):
    os.makedirs(app.config['UPLOAD_DIR'])


from app.tags_dict_init import get_tags_dict
td = get_tags_dict(app.config['RELOAD_TAGS_DICT_ON_START'])

from app.explanation_dicts_init import get_explanation_dict
ed = get_explanation_dict(app.config['EXPLANATION_DICTS'])

from app import routes
