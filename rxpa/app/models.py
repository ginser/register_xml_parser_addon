import functools
import inspect
import logging
import re
from datetime import datetime


from flask import g

from app import ed

logger = logging.getLogger('xml_parser')


class InspectionException(Exception):
    pass


class Error:
    def __init__(self, obj, **kwargs):
        self.message = kwargs.get('message')
        self.obj = obj

    def __repr__(self):
        return (f'c:\\nothing:{self.obj.line}:'
                f'{self.obj.column}: error: {self.message} - '
                f'{ed.get(self.message, "NOT FOUND EXPLANATION")}')


class AbsModel:
    def __init__(self, parent=None, root=None):
        self.parent = parent
        self.root = root
        self.content = None
        self.line = None
        self.column = None

    def prepare_aux_part(self, aux_dict, rule):
        self_name = type(self).__name__
        g.scf.obj_link = self

        if self_name != rule['main_iterator']:
            return

        self.aux = None
        try:
            main_iterator_key = g.S(rule['main_iterator_key'])
        except InspectionException as e:
            s = f'Not found iterator key: {rule["main_iterator_key"]}'
            logger.exception(s)
            self.root.report_file.write(s + f'. Error info: {repr(e)}\n')
            return

        aux_part = aux_dict.get(main_iterator_key, None)
        if aux_part is None:
            s = (f'Not found respective iterator with key: '
                 f'{rule["main_iterator"]} = {main_iterator_key}\n')
            logger.exception(s)
            self.root.report_file.write(s)
            return

        self.aux = AbsModel()
        setattr(self.aux, rule['aux_iterator'].split('/')[-1], aux_part)

    def make_inspections(self):
        g.scf.obj_link = self
        errors = []
        method_list = dict(inspect.getmembers(self, predicate=inspect.ismethod))
        for key, func in method_list.items():
            if 'inspection__' in key:
                value = log_inspection(func)
                if value is not None:
                    errors.append(value)
        return errors

    @classmethod
    def create_empty_child(cls, class_name):
        return type(class_name, (cls,), {})


class ShortConditionalFuncs:
    # функции для быстрого обращения к полю content и конвертации значения тега

    def __init__(self, obj_link=None):
        self.obj_link = obj_link

    def multi_getattr(self, obj, attr, default=None):
        attributes = attr.split(".")
        for i in attributes:
            try:
                obj = getattr(obj, i)
                if callable(obj):
                    obj = obj()
            except AttributeError:
                return default
        return obj

    # конвертация значения тега в int
    def I(self, tag=None):
        if tag is None:
            # если тег не указан, значение берется у родителя
            tag_obj = self.obj_link
        else:
            tag_obj = self.multi_getattr(self.obj_link, tag)

        if tag_obj is not None:
            try:
                return int(tag_obj.content)
            except (ValueError, TypeError) as e:
                return 0 #raise InspectionException from e
        else:
            raise InspectionException(f'Object {tag} not found')

    # конвертация значения тега в float
    def F(self, tag=None):
        if tag is None:
            # если тег не указан, значение берется у родителя
            tag_obj = self.obj_link
        else:
            tag_obj = self.multi_getattr(self.obj_link, tag)

        if tag_obj is not None:
            try:
                return float(tag_obj.content)
            except (ValueError, TypeError) as e:
                raise InspectionException from e
        else:
            raise InspectionException(f'Object {tag} not found')

    # получение строкового значения тега
    def S(self, tag=None):
        if tag is None:
            # если тег не указан, значение берется у родителя
            tag_obj = self.obj_link
        else:
            tag_obj = self.multi_getattr(self.obj_link, tag)

        if tag_obj is not None:
            return tag_obj.content
        else:
            raise InspectionException(f'Object {tag} not found')

    # Получение даты
    def D(self, tag=None):
        if tag is None:
            # если тег не указан, значение берется у родителя
            tag_obj = self.obj_link
        else:
            tag_obj = self.multi_getattr(self.obj_link, tag)

        if tag_obj is not None:
            try:
                date_out = datetime.strptime(tag_obj.content, "%Y-%m-%d").date()
                return date_out
            except (ValueError, TypeError) as e:
                raise InspectionException from e
        else:
            raise InspectionException(f'Object {tag} not found')

    # проверка на существование тега
    def E(self, tag):
        tag_obj = self.multi_getattr(self.obj_link, tag)
        return True if tag_obj is not None else False

    # проверка на отсутствие тега
    def NE(self, tag):
        return not self.E(tag)

    # облегчение работы с мкб строками вида C00
    def MKB(self, tag):
        return MKB_class(self, tag)


@functools.total_ordering
class MKB_class:  # диагноз. строки вида C00, C09
    def __init__(self, scf_obj, tag):
        char_range = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self.scf_obj = scf_obj

        try:
            self.char = self.scf_obj.S(tag)[0]
            self.number = int(self.scf_obj.S(tag)[1:3])
            if not (self.char in char_range and 0 <= self.number <= 99):
                raise Exception
        except Exception as e:
            raise InspectionException(
                f'Object {tag} is not in MKB format') from e

    def __eq__(self, other):
        return self.char == other[0] and self.number == int(other[1:3])

    def __le__(self, other):
        return self.char == other[0] and self.number < int(other[1:3])


def log_inspection(func):
    value = None
    cls_name = type(func.__self__).__name__
    try:
        value = func()
    except Exception as e:
        s = (f'{cls_name}, {func.__name__} has crashed. '
             f'Extra info: {Error(func.__self__)}')
        logger.exception(s)
        func.__self__.root.report_file.write(s + f'. Error info: {repr(e)}\n')
    else:
        if value is None:
            logger.info(
                (f'{cls_name}, {func.__name__} has passed '
                 f'successfully'))
        elif value == 'skipped':
            value = None
        else:
            logger.info(
                (f'{cls_name}, {func.__name__} has passed '
                 f'with error: {value}'))
    return value


# указывает документы, к которым применяется инспекция
def applicable_docs(doc_list):
    if isinstance(doc_list, str):
        doc_list = doc_list.split(', ')

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            prepared_doc_list = '|'.join(f'(?:{doc})' for doc in doc_list)
            if re.match(prepared_doc_list, args[0].root.filename) is None:
                logger.debug(
                    f'Skipping {type(args[0]).__name__}, {func.__name__}')
                return 'skipped'
            return func(*args, **kwargs)

        return wrapper

    return decorator


# указывает на то, что проверка выполняется при проверке сразу двух файлов
def join_file_inspection(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if hasattr(args[0], 'aux'):
            return func(*args, **kwargs)
    return wrapper
