import os
from io import StringIO
from xml.etree import ElementTree as ET
from flask import g
from app import app
from app.xml_checker import AuxXMLchecker


def get_xml_tree(elem, aux_iterator_tag, filename=None):
    elem_file = StringIO()
    ET.ElementTree(elem).write(elem_file, encoding='unicode')
    elem_file.seek(0)
    xmlchecker = AuxXMLchecker(filename=filename)
    xmlchecker.check_xml(xml_file=elem_file)
    elem_file.close()

    elem_obj = getattr(xmlchecker.handler.root, aux_iterator_tag)
    return elem_obj


def parse_aux_file(aux_file, rule):
    parsed_dict = dict()
    aux_iterator_tag = rule['aux_iterator'].split('/')[-1]

    with open(aux_file, encoding='cp1251') as f:
        root = ET.fromstring(f.read())
    for elem in root.findall(rule['aux_iterator']):
        elem_obj = get_xml_tree(
            elem, aux_iterator_tag,
            filename=f'aux_{os.path.basename(aux_file)}'
        )
        g.scf.obj_link = elem_obj
        parsed_dict[g.S(rule['aux_iterator_key'])] = elem_obj

    return parsed_dict
